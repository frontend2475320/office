import { createApp } from "vue";
import App from "./App.vue";
import router from "./router/index";
import store from "./store";
import { createI18n } from "vue-i18n";

/* 語系載入 */
import zh_chs from "./i18n/zh-CN.json";
import zh_cht from "./i18n/zh-TW.json";
import en from "./i18n/en-US.json";
import vn from "./i18n/vi-VN.json";
import pt from "./i18n/pt.json";
import ja_jp from "./i18n/ja_jp.json";
import hi_in from "./i18n/hi_in.json";
import id_id from "./i18n/id_id.json";
// en vn zh_chs zh_cht


//載入loading套件全域使用
import { LoadingPlugin } from "vue-loading-overlay";
import "vue-loading-overlay/dist/css/index.css";

//分頁套件
import VueAwesomePaginate from "vue-awesome-paginate";
import "vue-awesome-paginate/dist/style.css";

import "bootstrap";
/* import the fontawesome core */
import { library } from "@fortawesome/fontawesome-svg-core";



// 提示視窗
import VueSweetalert2 from "vue-sweetalert2";
import "sweetalert2/dist/sweetalert2.min.css";

/* import font awesome icon component */
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

/* import specific icons */
import { fas } from "@fortawesome/free-solid-svg-icons";
/* add icons to the library */
library.add(fas);

import { createPinia } from "pinia";

import vue3GoogleLogin from 'vue3-google-login'



const i18n = createI18n({
  legacy: false,
  locale: localStorage.getItem("locale") ?? "en",
  fallbackLocale: "en",
  messages: {
    zh_chs: zh_chs,
    zh_cht: zh_cht,
    en: en,
    vn: vn,
    pt: pt,
    ja_jp: ja_jp,
    hi_in: hi_in,
    id_id: id_id

  },
});

const pinia = createPinia();

createApp(App)
  .use(store)
  .use(router)
  .use(LoadingPlugin)
  .use(i18n)
  .use(VueAwesomePaginate)
  .use(VueSweetalert2)
  .use(pinia)
  .use(vue3GoogleLogin, {
    clientId: window.webconfig.googleKey
  }) 
  .component("font-awesome-icon", FontAwesomeIcon)
  .mount("#app");
