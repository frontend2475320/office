import apiService from './apiService'
import common from "@/utils/common";
import Swal from "sweetalert2";
import { useI18n } from "vue-i18n";
import { Modal } from 'bootstrap';


export default function () {
    const { errorHandling } = common();
    const { t } = useI18n();

    /// 取得优惠类型
    async function getApiPromotionTypeData(locale) {
        try {
            const response = await apiService.post('/Promotion/Types', {
                lang: locale
            });
            return response.data;
        } catch (error) {
            console.error(error);
        }
    }
    //取得優惠活動
    async function getApiPromotionListData(DomainLayoutId) {
        var params = {
            lang: localStorage.getItem("locale"),
            deviceType: 1,//桌機
            DomainLayoutId: DomainLayoutId
        };
        
        if (localStorage.getItem("sessionKey")) {
            params.sessionKey = localStorage.getItem("sessionKey");
        }
        
        try{
            const response = await apiService.post('/Promotion/List', params);
            return response.data.datas
        }
        catch(error){
            errorHandling(error);
        }
    }

    //申請按鈕
    async function promotionApply (selectedPromotionListNo, remark){
        if (!selectedPromotionListNo) {
            Swal.fire("", t("pls_select_offer"), "warning");
            return
        }
    
        const params = {
            sessionKey: localStorage.getItem("sessionKey"),
            no: selectedPromotionListNo,
            currencyNo: +(localStorage.getItem("currencyNo")),
            remark: remark
        }
    
        await apiService.post('/Promotion/Apply', params)
        .then(() => {
            Swal.fire("", t("successfulApplication"), "info");
            Modal.getInstance("#promoApplyPop")?.hide();
        }).
        catch((err) => {
            if (err.response == null || err.response.status == null) {
            console.log(err);
            Swal.fire("", t("sportApiThirdPartyCode_systemError"), "error");
            } else if (err.response.status == 452) {
            Swal.fire("", t("repeatApplication"), "warning");
            } else if (err.response.status == 453) {
            Swal.fire("", t("alreadyParticipated"), "warning");
            } else if (err.response.status == 454) {
            Swal.fire("", t("other_activities_in_application"), "warning");
            } else errorHandling(err);
        });
    
    }


    return { getApiPromotionListData, getApiPromotionTypeData, promotionApply }
}



