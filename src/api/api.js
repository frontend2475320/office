import apiService from './apiService'
import { Modal } from 'bootstrap';

/// 執行遊戲
export async function startGame(locale, gameThirdPartyNo, thirdPartyGameNo, thirdPartyTableNo) {
    const currencyNo = localStorage.getItem("currencyNo");
    const sessionKey = localStorage.getItem("sessionKey");

    if(!currencyNo || !sessionKey){
        if(Modal.getInstance("#collapseTarget")==null){
            let m = new Modal("#collapseTarget");
            m.show();
        }else{
            Modal.getInstance("#collapseTarget")?.show();
        }
    } else{
        const g = await gameLink(sessionKey, locale, currencyNo, gameThirdPartyNo, thirdPartyGameNo, thirdPartyTableNo);
        if (g != undefined && g.gameUrl != undefined) {
            const url = encodeURIComponent(g.gameUrl);
            window.open("/#/Load?u=" + url, "game");
        }
    }
}

/// 取得遊戲入口
export async function gameLink(sessionKey, locale, currencyNo, gameThirdPartyNo, thirdPartyGameNo, thirdPartyTableNo) {
    const response = await apiService.post('/Game/Url', {
        sessionKey: sessionKey,
        langId: locale,
        currencyNo: currencyNo,
        gameThirdPartyNo: gameThirdPartyNo,
        thirdPartyGameNo: thirdPartyGameNo,
        thirdPartyTableNo: thirdPartyTableNo,
    });
    var _json = JSON.parse(JSON.stringify(response.data));
    return _json;
}

/// 取得优惠类型
export async function getApiPromotionTypeData(locale) {
    try {
        const response = await apiService.post('/Promotion/Types', {
            lang: locale
        });
        var _json = JSON.parse(JSON.stringify(response.data));
        return _json;
    } catch (error) {
        console.error(error);
    }
}

/// 取得公告
export async function noticeList(locale, DomainLayoutId) {
    try {
        const response = await apiService.post('/Notice/List', {
            lang: locale,
            DomainLayoutId: DomainLayoutId
        });
        var _json = JSON.parse(JSON.stringify(response.data));
        return _json;
    } catch (error) {
        console.error(error);
    }
}

