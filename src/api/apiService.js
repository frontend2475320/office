import axios from 'axios';

const apiService = axios.create({
    baseURL: window.webconfig.url
});

export default apiService;