import { createStore } from 'vuex';
import apiService from '@/api/apiService';
import cnImg from "@/assets/img/lang/lang-cn.png";


export default createStore({
  state: {
    user: {
      bucketId: '',
      account: '',
      createTime: '',
      realName: '',
      avatarImage: '',
      nickName: '',
      aboutMe: '',
      birthday: '',
      gender: '',
      isAnchor: '',
      level: '',
      phoneNumber: '',
      email: '',
      directLowerCount: '',
      indirectLowerCount: '',
      type: '',
      recommendCode: '',
      recRegisterUrl: '',
      isPlatformPayOpen: '',
      isAgentPayOpen: '',
      hasTransactionPassword: '',
      peerageLevel: '',
      password: '',

      phoneCountryCode: '',
      verifyCode: '',
      gameThirdPartyID: '',
      deviceType: '',
    },
    wallets: [],
    isLogin: false,

    lang: {
      key: 'zh_chs',
      name: '简体中文',
      image: cnImg,
    },
    currency: {
      no: -1,
      currencyId: '',
      protocol: '',
      isVirtual: false,
    },
    balance: 0,
    showLoginTab: true,
    currencySelect: '', //給PhoneTransactionDetails用
    siteSettingsInfo: {
      apkLink: '',
      iosAppLink: '',
      customerServiceUrl: '',
      email: '',
      phoneNumber: '',
      downLoadUrl: '',
      isTelRegister: '',
      isTelLogin: '',
      isUpPhoneVerifyCode: '',
      isRegisterTelRequired: '',
      isRegisterEmailRequired: '',
      isRegisterRealNameRequired: '',
      planType: '',
      langIDs: ''
    },
    matchedLangs: [],
    DomainLayoutId: ''
  },
  mutations: {
    setLogin(state) {
      state.isLogin = true;
    },
    setLevel(state, level) {
      state.user.level = level;
      localStorage.setItem('level', level);
    },
    setAvatarImage(state, img) {
      state.user.avatarImage = img;
      localStorage.setItem('avatarImage', img);
    },
    setNickName(state, nickName) {
      state.user.nickName = nickName;
      localStorage.setItem('nickName', nickName);
    },
    setLogout(state) {
      let tempCurrencyNo = localStorage.getItem('currencyNo') ?? "";
      let tempCurrencyId = localStorage.getItem('currencyId') ?? "";
      let tempLocale = localStorage.getItem('locale') ?? "";
      localStorage.clear();

      if (tempLocale != "")
        localStorage.setItem('locale', tempLocale);

      if (tempCurrencyNo != "")
        localStorage.setItem('currencyNo', tempCurrencyNo);

      if (tempCurrencyId != "")
        localStorage.setItem('currencyId', tempCurrencyId);

      if (state.user.password) {
        localStorage.setItem('account', state.user.account);
        localStorage.setItem('password', state.user.password);
      }


      state.isLogin = false;
      state.balance = 0;

      state.user.bucketId = '';
      state.user.account = '';
      state.user.createTime = '';
      state.user.realName = '';
      state.user.avatarImage = '';
      state.user.nickName = '';
      state.user.aboutMe = '';
      state.user.birthday = '';
      state.user.gender = '';
      state.user.isAnchor = '';
      state.user.level = '';
      state.user.phoneNumber = '';
      state.user.email = '';
      state.user.directLowerCount = '';
      state.user.indirectLowerCount = '';
      state.user.type = '';
      state.user.recommendCode = '';
      state.user.recRegisterUrl = '';
      state.user.isPlatformPayOpen = '';
      state.user.isAgentPayOpen = '';
      state.user.hasTransactionPassword = '';
      state.user.peerageLevel = '';
      state.user.password = ''



    },
    setUserData(state, data) {
      state.user = data;
    },
    setIsLogin(state, data) {
      state.isLogin = data;
      const account = localStorage.getItem('account');
      const level = localStorage.getItem('level');
      let avatarImage = localStorage.getItem('avatarImage');
      if (avatarImage === 'null') {
        avatarImage = null;
      }
      const nickName = localStorage.getItem('nickName');
      if (account) {
        state.user.account = account;
        state.user.avatarImage = avatarImage;
        state.user.nickName = nickName;
        state.user.level = level;
      }
    },
    setLang(state, data) {
      state.lang = data;
      localStorage.setItem('locale', data.key);
    },
    setWalletMainInfo(state, data) {
      state.currency = data.currency;
      state.balance = data.balance;
      localStorage.setItem('currencyNo', data.currency.no);
      localStorage.setItem('currencyId', data.currency.currencyId);
    },
    serShowLoginTab(state, value) {
      state.showLoginTab = value;
    },
    setCurrencySelect(state, data) {
      state.currencySelect = data;
    },
    setSiteSettingsInfo(state, siteSettingsInfo) {
      state.siteSettingsInfo = siteSettingsInfo;
    },
    setUserPassword(state, password) {
      state.user.password = password;
    },
    setPasswordEmpty(state) {
      state.user.password = '';
    },
    setCurrency(state, currency) {
      state.currency = currency
    },
    setDomainLayoutId(state, DomainLayoutId) {
      state.DomainLayoutId = DomainLayoutId
    },
    setWallets(state, wallets) {
      localStorage.setItem('currencyNo', wallets[0].currency.no);
      localStorage.setItem('currencyId', wallets[0].currency.currencyId);
      state.wallets = wallets;
    },
    setMatchedLangs(state, matchedLangs) {
      state.matchedLangs = matchedLangs;
    }
  },
  actions: {
    //接登入API使用者資料
    async login({ commit }, userData) {
      try {
        commit('setLogin');
        commit('setUserData', userData);
      } catch (error) {
        console.error(error);
      }
    },
    //查詢主錢包資訊
    async walletMainInfo({ commit }) {
      try {
        const response = await apiService.post('/Wallet/Main/Info', {
          sessionKey: localStorage.getItem('sessionKey'),
          DomainLayoutId: this.state.DomainLayoutId
        });
        commit('setWalletMainInfo', response.data);
        return response.data;

      } catch (error) {
        if (error.response.status === 401) {
          commit('setLogout');
        }
        else {
          console.error(error);
        }
      }
    },
    setLevel({ commit }, level) {
      commit('setLevel', level);
    },
    setAvatarImage({ commit }, img) {
      commit('setAvatarImage', img);
    },
    setNickName({ commit }, nickName) {
      commit('setNickName', nickName);
    },
    //使用者登出
    setLogout({ commit }) {
      commit('setLogout');
    },
    setIsLogin({ commit }, _isLogin) {
      commit('setIsLogin', _isLogin);
    },
    setLang({ commit }, lang) {
      commit('setLang', lang);
    },
    serShowLoginTab({ commit }, value) {
      commit('serShowLoginTab', value);
    },
    setCurrencySelect({ commit }, currencyNo) {
      commit('setCurrencySelect', currencyNo);
    },
    setUserData({ commit }, UserData) {
      commit('setLogin');
      commit('setUserData', UserData)
    },
    setSiteSettingsInfo({ commit }, siteSettingsInfo) {
      commit('setSiteSettingsInfo', siteSettingsInfo);
    },
    setUserPassword({ commit }, password) {
      commit('setUserPassword', password);
    },
    setPasswordEmpty({ commit }) {
      commit('setPasswordEmpty');
    },
    setCurrency({ commit }, currency) {
      commit('setCurrency', currency);
    },
    setDomainLayoutId({ commit }, DomainLayoutId) {
      commit('setDomainLayoutId', DomainLayoutId);
    },
    setWallets({ commit }, wallets) {
      commit('setWallets', wallets);
    },
    setMatchedLangs({ commit }, matchedLangs) {
      commit('setMatchedLangs', matchedLangs);
    }
  },
  modules: {},
});
