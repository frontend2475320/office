import { defineStore } from 'pinia'
import apiService from '@/api/apiService';

export const useGamesStore = defineStore('games', {
    state: () => ({
        gameThirdPartyId: "",
        gameClass: -1,
        gameThirdPartyNo: -1,
        headerMenu: [],
        apiHeaderMenu: [],
        gameMenu: [
            {
                clickAction: -1,
                gameThirdPartyNo: -1,
                id: '',
                name: '',
                bimage: '',
                simage: '',
                sortStatus: -1,
                status: -1,
                games: [],
                pcLobbyImg: '',
                pcLogoImg: '',
                pcMenuImg: '',
            },
        ],
        phoneMenu: [],
        apiPhoneMenu:[]
    }),
    setup: () => {
        this.gameClass = localStorage.getItem('gameClass');
        this.gameThirdPartyNo = localStorage.getItem('gameThirdPartyNo');
    },
    actions: {
        /// 更新清單(多語系)
        async getGameList(t, DomainLayoutId) {
            //主選單
            this.apiHeaderMenu = [
                {
                    no: -1,
                    key: 'home',
                    name: t('home'),
                    path: '',
                },
                {
                    no: 4,
                    key: 'sport_game',
                    name: t('sport_game'),
                    path: 'Sports',
                    showSubMenu: 'show-sub',
                    news: true,
                    hover: '',
                    subMenu: [],
                },
                {
                    no: 2,
                    key: 'live_game',
                    name: t('live_game'),
                    path: 'Casino',
                    showSubMenu: 'show-sub',
                    news: true,
                    hover: '',
                    subMenu: [],
                },
                {
                    no: 3,
                    key: 'electronic_games',
                    name: t('elec'),
                    path: 'Egame',
                    showSubMenu: 'show-sub',
                    hover: '',
                    subMenu: [],
                    dirIn:true
                },
                {
                    no: 1,
                    key: 'lottery_game',
                    name: t('lottery_game'),
                    path: 'Lottery',
                    showSubMenu: 'show-sub',
                    hot: true,
                    hover: '',
                    subMenu: [],
                },
                {
                    no: 6,
                    key: 'fishing',
                    name: t('fishing'),
                    path: 'Fish',
                    showSubMenu: 'show-sub',
                    hover: '',
                    subMenu: [],
                },
                {
                    no: 5,
                    key: 'chess_and_board_games',
                    name: t('chess'),
                    path: 'Chess',
                    showSubMenu: 'show-sub',
                    hover: '',
                    subMenu: [],
                    dirIn:true
                },
                {
                    no: 7,
                    key: 'sport_event',
                    name: t('sportEvents'),
                    path: 'SportEvent',
                    showSubMenu: 'show-sub',
                    hover: '',
                    subMenu: [],
                },
                {
                    no: -1,
                    key: 'promotions',
                    name: t('promotions'),
                    path: 'Promotion',
                    hover: '',
                    hot: true,
                },
                {
                    no: -1,
                    key: 'alternateURL',
                    name: t('alternateURL'),
                    path: 'AppDownload',
                    hover: '',
                },
            ];
            try {
                let resp = await apiService.post('/Game/GameThirdParty/List', { lang: localStorage.getItem('locale'), DomainLayoutId: DomainLayoutId })
                
                var _json = JSON.parse(JSON.stringify(resp.data));
                const gameClasses = _json.datas;
                for (let i = 0; i < gameClasses.length; i++) {
                    const gameClass = gameClasses[i];
                    for (let j = 0; j < this.apiHeaderMenu.length; j++) {
                        const menu = this.apiHeaderMenu[j];
                        if (menu.no != -1 && gameClass.gameClassNo == menu.no) {
                            menu.subMenu = gameClass.gameThirdParties;
                            continue;
                        }
                    }
                }

                this.filterHeaderMenuByCurrency();

                //設定遊戲類別
                let gameClassNo = localStorage.getItem('gameClass');
                let gameThirdPartyNo = localStorage.getItem('gameThirdPartyNo');
                let gameThirdPartyId = localStorage.getItem('gameThirdPartyId');
                gameThirdPartyNo = (gameThirdPartyNo) ?? -1;
                if (gameClassNo && gameClassNo > -1) {
                    this.setGameThirdPartyNo(gameClassNo, gameThirdPartyNo, gameThirdPartyId, 'gameThirdParty');
                }
            } catch (error) {
                console.log(error);
            }
        },
        filterHeaderMenuByCurrency(){
            //先塞API抓來的初始資料給headerMenu
            this.headerMenu = [];
            this.apiHeaderMenu.forEach(gameClass => {
                const {no, key, name, hover, news, path, showSubMenu, subMenu, dirIn } = gameClass
                this.headerMenu.push({
                    no, key, name, hover, news, path, showSubMenu, subMenu, dirIn
                })
            })
            
            const currencyNo = +localStorage.getItem("currencyNo");

            this.headerMenu.forEach((gameClass, index) => {
                if(gameClass.subMenu){
                    this.headerMenu[index].subMenu = gameClass.subMenu.filter(submenuItem => submenuItem.currencyNos.includes(currencyNo))
                }
            })
            this.headerMenu = this.headerMenu.filter(gameClass => !gameClass.subMenu || gameClass.subMenu.length > 0);
        },
        async getPhoneGameList(t, DomainLayoutId) {
            //主選單
            this.apiPhoneMenu = [
                {
                    no: 0,
                    key: 'hot_game',
                    name: t('hot'),
                    subMenu: [],
                },
                {
                    no: 1,
                    key: 'lottery_game',
                    name: t('lottery_game'),
                    subMenu: [],
                },
                {
                    no: 2,
                    key: 'live_game',
                    name: t('live_game'),
                    subMenu: [],
                },
                {
                    no: 3,
                    key: 'electronic_games',
                    name: t('elec'),
                    subMenu: [],
                },
                {
                    no: 4,
                    key: 'sport_game',
                    name: t('sport_game'),
                    subMenu: [],
                },
                {
                    no: 5,
                    key: 'chess_and_board_games',
                    name: t('chess'),
                    subMenu: [],
                },

                {
                    no: 6,
                    key: 'fishing',
                    name: t('fishing'),
                    subMenu: [],
                },

                {
                    no: 7,
                    key: 'sport_event',
                    name: t('sportEvents'),
                    subMenu: [],
                },

            ];
            try {
                let resp = await apiService.post('/Game/GameThirdParty/List', { lang: localStorage.getItem('locale'), DomainLayoutId: DomainLayoutId })
                
                var _json = JSON.parse(JSON.stringify(resp.data));
                const gameClasses = _json.datas;

                this.apiPhoneMenu[0].subMenu = [];//清空熱門分類重抓資料
                for (let i = 0; i < gameClasses.length; i++) {
                    for (let j = 0; j < this.apiPhoneMenu.length; j++) {
                        if (gameClasses[i].gameClassNo == this.apiPhoneMenu[j].no) {
                            this.apiPhoneMenu[j].subMenu = gameClasses[i].gameThirdParties;
                        }
                    }
                    //sortStatus === 1 為熱門，放入，放入第一個phoneMenu
                    for (let k = 0; k < gameClasses[i].gameThirdParties.length; k++) {
                        if (gameClasses[i].gameThirdParties[k].sortStatus === 1) {
                            this.apiPhoneMenu[0].subMenu.push(gameClasses[i].gameThirdParties[k])
                        }
                    }
                }
                this.filterPhoneHeaderMenuByCurrency();
            } catch (error) {
                console.log(error);
            }
        },

        filterPhoneHeaderMenuByCurrency(){
            //先塞API抓來的初始資料給headerMenu
            this.phoneMenu = [];
            this.apiPhoneMenu.forEach(gameClass => {
                const {no, key, name, subMenu } = gameClass
                this.phoneMenu.push({
                    no, key, name, subMenu
                })
            })
            
            const currencyNo = +localStorage.getItem("currencyNo");
            
            this.phoneMenu.forEach((gameClass, index) => {
                this.phoneMenu[index].subMenu = gameClass.subMenu.filter(submenuItem => submenuItem.currencyNos.includes(currencyNo))
            })

            this.phoneMenu = this.phoneMenu.filter(gameClass => gameClass.subMenu.length > 0);
        },
        /// 設定遊戲館
        setGameThirdPartyNo(no, gameThirdPartyNo, gameThirdPartyId, type) {
            
            this.gameThirdPartyNo = Number(gameThirdPartyNo);
            this.gameThirdPartyId = gameThirdPartyId;
            let hasMatched = false;
            
            //pc板
            if(this.headerMenu.length > 0){
                for (let i = 0; i < this.headerMenu.length; i++) {
                    const gameClass = this.headerMenu[i];
                    if (gameClass.no == no) {
                        this.gameMenu = gameClass.subMenu;
                        this.gameClass = Number(no);
                        localStorage.setItem('gameClass', no);
                        hasMatched = true;
                        break;
                    }
                }
            }
            //h5版
            else{
                for (let i = 0; i < this.phoneMenu.length; i++) {
                    const gameClass = this.phoneMenu[i];
                    if (gameClass.no == no) {
                        this.gameMenu = gameClass.subMenu;
                        this.gameClass = Number(no);
                        localStorage.setItem('gameClass', no);
                        hasMatched = true;
                        break;
                    }
                }
            }
            if(!hasMatched){
                return false;
            }
            //若未設定館別，先取有開啟的，再取第一順位
            if (this.gameThirdPartyNo === -1) {
                if(type === 'gameHall'){
                    
                    for (let i = this.gameMenu.length - 1; i >= 0; i--) {
                        let g = this.gameMenu[i];
                        if (g.status === 0 && g.clickAction == 1) {
                            this.gameThirdPartyNo = g.gameThirdPartyNo;
                            this.gameThirdPartyId = g.id;
                        }
                    }
                }
                else if(type === 'gameThirdParty'){
                    for (let i = this.gameMenu.length - 1; i >= 0; i--) {
                        let g = this.gameMenu[i];
    
                        if (g.status === 0) {
                            this.gameThirdPartyNo = g.gameThirdPartyNo;
                            this.gameThirdPartyId = g.id;
                        }
                    }
                }
            }

            if(this.gameThirdPartyNo !== -1){
                localStorage.setItem('gameThirdPartyNo', this.gameThirdPartyNo);
                localStorage.setItem('gameThirdPartyId', this.gameThirdPartyId);
            }
            else{
                localStorage.removeItem('gameThirdPartyNo');
                localStorage.removeItem('gameThirdPartyId');
            }
        },



        //aab
        async getGameListForMe88(t, lang, DomainLayoutId) {
            //主選單
            this.headerMenu = [
                {
                    no: -1,
                    key: 'home',
                    name: t('home'),
                    path: '',
                },
                {
                    no: 4,
                    key: 'sport_game',
                    name: t('sport_game'),
                    path: 'sport',
                    subMenu: [],
                },
                {
                    no: 7,
                    key: 'sport_event',
                    name: t('sportEvents'),
                    path: 'esports',
                    subMenu: [],
                },
                {
                    no: 2,
                    key: 'live_game',
                    name: t('live_game'),
                    path: 'casino',
                    subMenu: [],
                    hot: true,
                },
                {
                    no: 3,
                    key: 'electronic_games',
                    name: t('elec'),
                    path: 'gameHall',
                    subMenu: [],
                    hot: true,
                },
                {
                    no: 5,
                    key: 'chess_and_board_games',
                    name: t('chess'),
                    path: 'chess',
                    subMenu: [],
                },
                {
                    no: 6,
                    key: 'fishing',
                    name: t('fishing'),
                    path: 'gameHall',
                    subMenu: [],
                },
                {
                    no: 1,
                    key: 'lottery_game',
                    name: t('lottery_game'),
                    path: 'gameHall',
                    subMenu: [],
                },
                {
                    no: -1,
                    key: 'sponsor',
                    name: t('sponsor'),
                    path: 'sponsorship',
                },

                {
                    no: -1,
                    key: 'promotions',
                    name: t('promotions'),
                    path: 'promotion',
                },
                {
                    no: -1,
                    key: 'vip',
                    name: t('vip_privileges'),
                    path: 'vip',
                },
                {
                    no: -1,
                    key: 'leaderboard',
                    name: t('leaderboard'),
                    path: 'leaderboard',
                },
            ];
            try {
                if (!lang) {
                    lang = localStorage.getItem('locale');
                }
                // const currencyNo = localStorage.getItem('currencyNo');
                apiService
                    .post('/Game/List', { lang: lang, DomainLayoutId: DomainLayoutId })
                    .then((value) => {
                        //更新清單(多語系)
                        var _json = JSON.parse(JSON.stringify(value.data));
                        const gameClasses = _json.datas;
                        for (let i = 0; i < gameClasses.length; i++) {
                            const gameClass = gameClasses[i];
                            for (let j = 0; j < this.headerMenu.length; j++) {
                                const menu = this.headerMenu[j];
                                if (menu.no != -1 && gameClass.gameClassNo == menu.no) {
                                    menu.subMenu = gameClass.gameThirdParties;
                                    continue;
                                }
                            }
                        }
                        //設定遊戲類別
                        let gameClassNo = localStorage.getItem('gameClass');
                        let gameThirdPartyNo = localStorage.getItem('gameThirdPartyNo');
                        let gameThirdPartyId = localStorage.getItem('gameThirdPartyId');
                        gameThirdPartyNo = (gameThirdPartyNo) ?? -1;
                        if (gameClassNo && gameClassNo > -1) {
                            this.setGameThirdPartyNo(gameClassNo, gameThirdPartyNo, gameThirdPartyId);
                        }
                    });
            } catch (error) {
                console.log(error);
            }
        },
    },
})