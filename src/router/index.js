import { createRouter, createWebHashHistory } from "vue-router";
const layouts = window.webconfig.layouts
const routes = [
  {
    path: "/",
    name: "LoadingPage",
    component: () => import("../pages/LoadingView.vue"),
  },
  // aaa
  {
    path: "/aaa",
    component: () => import("../layout/aaa/LayoutView.vue"),
    beforeEnter: (to, from, next) => {
      if (layouts.includes("aaa")) {
        const screenWidth = window.innerWidth;
        if (screenWidth < 720) {
          next("/aaa/phone");
        } else {
          next();
        }
      }
      else {
        next("/");
      }
    },
    children: [
      {
        // 首頁
        path: "/aaa",
        name: "aaaHome",
        component: () => import("../pages/aaa/HomeView.vue"),
      },
      {
        // 體育
        path: "/aaa/Sports",
        name: "aaaSports",
        component: () => import("../pages/aaa/SportView.vue"),
      },
      {
        // 體育大廳
        path: "/aaa/SportHall",
        name: "aaaSportHall",
        component: () => import("../pages/aaa/SportHallView.vue"),
      },
      {
        // 真人
        path: "/aaa/Casino",
        name: "aaaCasino",
        component: () => import("../pages/aaa/CasinoView.vue"),
      },
      {
        // 真人大廳
        path: "/aaa/CasinoHall",
        name: "aaaCasinoHall",
        component: () => import("../pages/aaa/CasinoHallView.vue"),
      },
      {
        // 電子
        path: "/aaa/Egame",
        name: "aaaEgame",
        component: () => import("../pages/aaa/EgameView.vue"),
      },
      {
        // 彩票
        path: "/aaa/Lottery",
        name: "aaaLottery",
        component: () => import("../pages/aaa/LotteryView.vue"),
      },
      {
        // 彩票大廳
        path: "/aaa/LotteryHall",
        name: "aaaLotteryHall",
        component: () => import("../pages/aaa/LotteryHallView.vue"),
      },
      {
        // 捕魚
        path: "/aaa/Fish",
        name: "aaaFish",
        component: () => import("../pages/aaa/FishView.vue"),
      },
      {
        // 捕魚大廳
        path: "/aaa/FishHall",
        name: "aaaFishHall",
        component: () => import("../pages/aaa/FishHallView.vue"),
      },
      {
        // 棋牌
        path: "/aaa/Chess",
        name: "aaaChess",
        component: () => import("../pages/aaa/ChessView.vue"),
      },
      {
        // 運動
        path: "/aaa/SportEvent",
        name: "aaaSportEvent",
        component: () => import("../pages/aaa/SportEventView.vue"),
      },
      {
        // 運動大廳
        path: "/aaa/SportEventHall",
        name: "aaaSportEventHall",
        component: () => import("../pages/aaa/SportEventHallView.vue"),
      },
      {
        // 會員中心
        path: "/aaa/MemberCentre",
        name: "aaaMemberCentre",
        component: () => import("../components/aaa/MemberCentreView.vue"),
        children: [
          {
            // 帳戶與資料
            path: "/aaa/MemberCentre/UserInfo",
            name: "aaaUserInfo",
            redirect: "/aaa/MemberCentre/UserInfo/UserChangeInfo", // 預設基本資料
            component: () =>
              import("../components/memberCentre/userInfo/UserInfoNavView.vue"),
            children: [
              {
                // 基本資料
                path: "/aaa/MemberCentre/UserInfo/UserChangeInfo",
                name: "aaaUserChangeInfo",
                component: () =>
                  import(
                    "../components/memberCentre/userInfo/UserChangeInfoView.vue"
                  ),
              },
              {
                // 修改密碼
                path: "/aaa/MemberCentre/UserInfo/UserChangeLoginPw",
                name: "aaaUserChangeLoginPw",
                component: () =>
                  import(
                    "../components/memberCentre/userInfo/UserChangeLoginPwView.vue"
                  ),
              },
              {
                // 手機綁定
                path: "/aaa/MemberCentre/UserInfo/BindPhoneNumber",
                name: "aaaBindPhoneNumber",
                component: () =>
                  import(
                    "../components/memberCentre/userInfo/BindPhoneNumber.vue"
                  ),
              },
            ],
          },
          {
            // 充值
            path: "/aaa/MemberCentre/Deposit",
            name: "aaaDeposit",
            redirect: "/aaa/MemberCentre/Deposit/CommonDeposit", // 預設傳統充值
            component: () =>
              import("../components/memberCentre/deposit/DepositNavView.vue"),
            children: [
              {
                // 傳統充值
                path: "/aaa/MemberCentre/Deposit/CommonDeposit",
                name: "aaaCommonDeposit",
                component: () =>
                  import(
                    "../components/memberCentre/deposit/CommonDepositView.vue"
                  ),
              },
              {
                // 傳統充值紀錄
                path: "/aaa/MemberCentre/Deposit/DepositRecord",
                name: "aaaDepositRecord",
                component: () =>
                  import(
                    "../components/memberCentre/deposit/DepositRecordView.vue"
                  ),
              },
              {
                // 線上充值
                path: "/aaa/MemberCentre/Deposit/OnlineDeposit",
                name: "aaaOnlineDeposit",
                component: () =>
                  import(
                    "../components/memberCentre/deposit/OnlineDepositView.vue"
                  ),
              },
              {
                // 線上充值紀錄
                path: "/aaa/MemberCentre/Deposit/OnlineDepositRecord",
                name: "aaaOnlineDepositRecord",
                component: () =>
                  import(
                    "../components/memberCentre/deposit/OnlineDepositRecordView.vue"
                  ),
              },
            ],
          },
          {
            // 提領
            path: "/aaa/MemberCentre/Withdraw",
            name: "aaaWithdraw",
            redirect: "/aaa/MemberCentre/Withdraw/CommonWithdraw", // 預設傳統提款
            component: () =>
              import("../components/memberCentre/withdraw/WithdrawNavView.vue"),
            children: [
              {
                // 傳統提款
                path: "/aaa/MemberCentre/Withdraw/CommonWithdraw",
                name: "aaaCommonWithdraw",
                component: () =>
                  import(
                    "../components/memberCentre/withdraw/CommonWithdrawView.vue"
                  ),
              },
              {
                // 提款紀錄
                path: "/aaa/MemberCentre/Withdraw/WithdrawRecord",
                name: "aaaWithdrawRecord",
                component: () =>
                  import(
                    "../components/memberCentre/withdraw/WithdrawRecordView.vue"
                  ),
              },
              {
                // 綁定銀行卡
                path: "/aaa/MemberCentre/Withdraw/WithdrawBankBind",
                name: "aaaWithdrawBankBind",
                component: () =>
                  import(
                    "../components/memberCentre/withdraw/WithdrawBankBindView.vue"
                  ),
              },
              {
                // 修改轉帳密碼
                path: "/aaa/MemberCentre/Withdraw/ChangeWithdrawPwView",
                name: "aaaChangeWithdrawPwView",
                component: () =>
                  import(
                    "../components/memberCentre/withdraw/ChangeWithdrawPwView.vue"
                  ),
              },
            ],
          },
          {
            // VIP 特權
            path: "/aaa/MemberCentre/VIP",
            name: "aaaVIP",
            component: () => import("../components/memberCentre/VIPView.vue"),
          },
          {
            // 代理團隊
            path: "/aaa/MemberCentre/Agency",
            name: "aaaAgency",
            redirect: "/aaa/MemberCentre/Agency/AgencyTeam", // 預設代理團隊
            component: () =>
              import("../components/memberCentre/agency/AgencyNavView.vue"),
            children: [
              {
                // 代理團隊
                path: "/aaa/MemberCentre/Agency/AgencyTeam",
                name: "aaaAgencyTeam",
                component: () =>
                  import(
                    "../components/memberCentre/agency/AgencyTeamView.vue"
                  ),
              },
              {
                // 成員管理
                path: "/aaa/MemberCentre/Agency/AgencyMember",
                name: "aaaAgencyMember",
                component: () =>
                  import(
                    "../components/memberCentre/agency/AgencyMemberView.vue"
                  ),
              },
              {
                // 代理推廣
                path: "/aaa/MemberCentre/Agency/AgencyPromo",
                name: "aaaAgencyPromo",
                component: () =>
                  import(
                    "../components/memberCentre/agency/AgencyPromoView.vue"
                  ),
              },
              {
                // 傭金報表
                path: "/aaa/MemberCentre/Agency/AgencyRecord",
                name: "aaaAgencyRecord",
                component: () =>
                  import(
                    "../components/memberCentre/agency/AgencyRecordView.vue"
                  ),
              },
              {
                // 傭金方案
                path: "/aaa/MemberCentre/Agency/AgencyBonus",
                name: "aaaAgencyBonus",
                component: () =>
                  import(
                    "../components/memberCentre/agency/AgencyBonusView.vue"
                  ),
              },
            ],
          },
          {
            // 歷史紀錄
            path: "/aaa/MemberCentre/History",
            name: "aaaHistory",
            redirect: "/aaa/MemberCentre/History/HistoryStakeRecord", // 預設投注紀錄
            component: () =>
              import("../components/memberCentre/history/HistoryNavView.vue"),
            children: [
              {
                // 投注紀錄
                path: "/aaa/MemberCentre/History/HistoryStakeRecord",
                name: "aaaHistoryStakeRecord",
                component: () =>
                  import(
                    "../components/memberCentre/history/HistoryStakeRecordView.vue"
                  ),
              },
              {
                // 資金明細
                path: "/aaa/MemberCentre/History/TransactionDetails",
                name: "aaaTransactionDetails",
                component: () =>
                  import(
                    "../components/memberCentre/history/TransactionDetailsView.vue"
                  ),
              },
            ],
          },
          {
            // 站內信
            path: "/aaa/MemberCentre/Message",
            children: [
              {
                // 站內信
                path: "",
                name: "aaaMessage",
                component: () =>
                  import("../components/memberCentre/message/MessageView.vue"),
              },
              {
                // 站內信詳情
                path: "/aaa/MemberCentre/Message/MessageDetail",
                name: "aaaMessageDetail",
                component: () =>
                  import(
                    "../components/memberCentre/message/MessageDetailView.vue"
                  ),
              },
            ],
          },
        ],
      },
      {
        // 關於我們
        path: "/aaa/OtherPage",
        name: "aaaOtherPage",
        component: () => import("../pages/aaa/OtherPage.vue"),
      },

      {
        // 下載
        path: "/aaa/AppDownLoad",
        name: "aaaAppDownLoad",
        component: () => import("../pages/aaa/AppDownLoad.vue"),
      },
      {
        // 優惠活動
        path: "/aaa/Promotion",
        name: "aaaPromotion",
        component: () => import("../pages/aaa/PromotionView.vue"),
      },
    ],
  },
  // aaa 手機版
  {
    path: "/aaa/phone",
    component: () => import("../layout/aaa/PhoneView.vue"),
    beforeEnter: (to, from, next) => {
      if (layouts.includes("aaa")) {
        const screenWidth = window.innerWidth;
        if (screenWidth >= 720) {
          next("/aaa");
        } else {
          next();
        }
      }
      else {
        next("/");
      }
    },
    children: [
      {
        // 手機首頁
        path: "/aaa/phone",
        name: "aaaPhonehome",
        component: () => import("../pages/aaa/phone/PhoneHomeView.vue"),
      },
      {
        // 遊戲大廳(手機版)
        path: "/aaa/phone/PhoneGameHall",
        name: "aaaPhoneGameHall",
        component: () => import("../pages/aaa/phone/PhoneGameHallView.vue"),
      },
      {
        // 優惠(手機版)
        path: "/aaa/phone/Promotion",
        name: "aaaPhonePromotion",
        component: () => import("../pages/aaa/phone/PhonePromotionView.vue"),
      },
      {
        // 會員中心(手機版)
        path: "/aaa/phone/memberCentre",
        name: "aaaPhoneMemberView",
        component: () =>
          import("../components/aaa/PhoneMemberView.vue"),
      },
      {
        // 充值首頁
        path: "/aaa/phone/deposit/PhoneDepositNav",
        name: "aaaPhoneDepositNav",
        component: () =>
          import(
            "../components/aaa/phone/memberCentre/deposit/PhoneDepositNavView.vue"
          ),
      },
      {
        // 取款首頁
        path: "/aaa/phone/withdraw/PhoneCommonWithdrawView",
        name: "aaaPhoneCommonWithdraw",
        component: () =>
          import(
            "../components/aaa/phone/memberCentre/withdraw/PhoneCommonWithdrawView.vue"
          ),
      },
      {
        // 資金明細(手機版)
        path: "/aaa/phone/memberCentre/history/PhoneTransactionDetails",
        name: "aaaPhoneTransactionDetails",
        component: () =>
          import(
            "../components/aaa/phone/memberCentre/history/PhoneTransactionDetailsView.vue"
          ),
      },
      {
        // 投注紀錄(手機版)
        path: "/aaa/phone/memberCentre/history/PhoneHistoryStakeRecord",
        name: "aaaPhoneHistoryStakeRecord",
        component: () =>
          import(
            "../components/aaa/phone/memberCentre/history/PhoneHistoryStakeRecordView.vue"
          ),
      },
      {
        // 選擇幣別(手機版)
        path: "/aaa/phone/PhoneCurrencySelect",
        name: "aaaPhoneCurrencySelect",
        component: () =>
          import("../components/aaa/phone/PhoneCurrencySelectVIew.vue"),
      },
      {
        // 代理加盟(手機版)
        path: "/aaa/phone/agency/PhoneAgency",
        name: "aaaPhoneAgency",
        component: () =>
          import(
            "../components/aaa/phone/memberCentre/agency/PhoneAgencyView.vue"
          ),
      },
      {
        // 代理加盟-成員管理(手機版)
        path: "/aaa/phone/agency/PhoneAgencyTeam",
        name: "aaaPhoneAgencyTeam",
        component: () =>
          import(
            "../components/aaa/phone/memberCentre/agency/PhoneAgencyTeamView.vue"
          ),
      },
      {
        // 代理加盟-代理推廣(手機版)
        path: "/aaa/phone/agency/PhoneAgencyPromo",
        name: "aaaPhoneAgencyPromo",
        component: () =>
          import(
            "../components/aaa/phone/memberCentre/agency/PhoneAgencyPromoView.vue"
          ),
      },
      {
        // 代理加盟-傭金方案(手機版)
        path: "/aaa/phone/agency/PhoneAgencyBonus",
        name: "aaaPhoneAgencyBonus",
        component: () =>
          import(
            "../components/aaa/phone/memberCentre/agency/PhoneAgencyBonusView.vue"
          ),
      },
      {
        // 代理加盟-傭金領取紀錄(手機版)
        path: "/aaa/phone/agency/PhoneAgencyBonusRecord",
        name: "aaaPhoneAgencyBonusRecord",
        component: () =>
          import(
            "../components/aaa/phone/memberCentre/agency/PhoneAgencyBonusRecordView.vue"
          ),
      },
      {
        // VIP頁(手機版)
        path: "/aaa/phone/memberCentre/PhoneVIPView",
        name: "aaaPhoneVIPView",
        component: () =>
          import("../components/aaa/PhoneVIPView.vue"),
      },
      {
        // 站內信(手機版)
        path: "/aaa/phone/memberCentre/message/PhoneMessage",
        name: "aaaPhoneMessage",
        component: () =>
          import(
            "../components/aaa/phone/memberCentre/message/PhoneMessageView.vue"
          ),
      },
      {
        // 站內信內容頁(手機版)
        path: "/aaa/phone/memberCentre/message/PhoneMessageDetail",
        name: "aaaPhoneMessageDetail",
        component: () =>
          import(
            "../components/aaa/phone/memberCentre/message/PhoneMessageDetailView.vue"
          ),
      },
      {
        // 設定
        path: "/aaa/phone/memberCentre/userInfo/PhoneUserChangeInfo",
        name: "aaaPhoneUserChangeInfoView",
        component: () =>
          import(
            "../components/aaa/phone/memberCentre/userInfo/PhoneUserChangeInfoView.vue"
          ),
      },
    ],
  },
  // aad
  {
    path: "/aad",
    component: () => import("../layout/aad/LayoutView.vue"),
    beforeEnter: (to, from, next) => {
      if (layouts.includes("aad")) {
        const screenWidth = window.innerWidth;
        if (screenWidth < 720) {
          next("/aad/phone");
        } else {
          next();
        }
      }
      else {
        next("/");
      }
    },
    children: [
      {
        // 首頁
        path: "/aad",
        name: "aadHome",
        component: () => import("../pages/aad/HomeView.vue"),
      },
      {
        // 體育
        path: "/aad/Sports",
        name: "aadSports",
        component: () => import("../pages/aad/SportView.vue"),
      },
      {
        // 體育大廳
        path: "/aad/SportHall",
        name: "aadSportHall",
        component: () => import("../pages/aad/SportHallView.vue"),
      },
      {
        // 真人
        path: "/aad/Casino",
        name: "aadCasino",
        component: () => import("../pages/aad/CasinoView.vue"),
      },
      {
        // 真人大廳
        path: "/aad/CasinoHall",
        name: "aadCasinoHall",
        component: () => import("../pages/aad/CasinoHallView.vue"),
      },
      {
        // 電子
        path: "/aad/Egame",
        name: "aadEgame",
        component: () => import("../pages/aad/EgameView.vue"),
      },
      {
        // 彩票
        path: "/aad/Lottery",
        name: "aadLottery",
        component: () => import("../pages/aad/LotteryView.vue"),
      },
      {
        // 彩票大廳
        path: "/aad/LotteryHall",
        name: "aadLotteryHall",
        component: () => import("../pages/aad/LotteryHallView.vue"),
      },
      {
        // 捕魚
        path: "/aad/Fish",
        name: "aadFish",
        component: () => import("../pages/aad/FishView.vue"),
      },
      {
        // 捕魚大廳
        path: "/aad/FishHall",
        name: "aadFishHall",
        component: () => import("../pages/aad/FishHallView.vue"),
      },
      {
        // 棋牌
        path: "/aad/Chess",
        name: "aadChess",
        component: () => import("../pages/aad/ChessView.vue"),
      },
      {
        // 運動
        path: "/aad/SportEvent",
        name: "aadSportEvent",
        component: () => import("../pages/aad/SportEventView.vue"),
      },
      {
        // 運動大廳
        path: "/aad/SportEventHall",
        name: "aadSportEventHall",
        component: () => import("../pages/aad/SportEventHallView.vue"),
      },
      {
        // 會員中心
        path: "/aad/MemberCentre",
        name: "aadMemberCentre",
        component: () => import("../components/aad/MemberCentreView.vue"),
        children: [
          {
            // 帳戶與資料
            path: "/aad/MemberCentre/UserInfo",
            name: "aadUserInfo",
            redirect: "/aad/MemberCentre/UserInfo/UserChangeInfo", // 預設基本資料
            component: () =>
              import("../components/memberCentre/userInfo/UserInfoNavView.vue"),
            children: [
              {
                // 基本資料
                path: "/aad/MemberCentre/UserInfo/UserChangeInfo",
                name: "aadUserChangeInfo",
                component: () =>
                  import(
                    "../components/memberCentre/userInfo/UserChangeInfoView.vue"
                  ),
              },
              {
                // 修改密碼
                path: "/aad/MemberCentre/UserInfo/UserChangeLoginPw",
                name: "aadUserChangeLoginPw",
                component: () =>
                  import(
                    "../components/memberCentre/userInfo/UserChangeLoginPwView.vue"
                  ),
              },
              {
                // 手機綁定
                path: "/aad/MemberCentre/UserInfo/BindPhoneNumber",
                name: "aadBindPhoneNumber",
                component: () =>
                  import(
                    "../components/memberCentre/userInfo/BindPhoneNumber.vue"
                  ),
              },
            ],
          },
          {
            // 充值
            path: "/aad/MemberCentre/Deposit",
            name: "aadDeposit",
            redirect: "/aad/MemberCentre/Deposit/CommonDeposit", // 預設傳統充值
            component: () =>
              import("../components/memberCentre/deposit/DepositNavView.vue"),
            children: [
              {
                // 傳統充值
                path: "/aad/MemberCentre/Deposit/CommonDeposit",
                name: "aadCommonDeposit",
                component: () =>
                  import(
                    "../components/memberCentre/deposit/CommonDepositView.vue"
                  ),
              },
              {
                // 傳統充值紀錄
                path: "/aad/MemberCentre/Deposit/DepositRecord",
                name: "aadDepositRecord",
                component: () =>
                  import(
                    "../components/memberCentre/deposit/DepositRecordView.vue"
                  ),
              },
              {
                // 線上充值
                path: "/aad/MemberCentre/Deposit/OnlineDeposit",
                name: "aadOnlineDeposit",
                component: () =>
                  import(
                    "../components/memberCentre/deposit/OnlineDepositView.vue"
                  ),
              },
              {
                // 線上充值紀錄
                path: "/aad/MemberCentre/Deposit/OnlineDepositRecord",
                name: "aadOnlineDepositRecord",
                component: () =>
                  import(
                    "../components/memberCentre/deposit/OnlineDepositRecordView.vue"
                  ),
              },
            ],
          },
          {
            // 提領
            path: "/aad/MemberCentre/Withdraw",
            name: "aadWithdraw",
            redirect: "/aad/MemberCentre/Withdraw/CommonWithdraw", // 預設傳統提款
            component: () =>
              import("../components/memberCentre/withdraw/WithdrawNavView.vue"),
            children: [
              {
                // 傳統提款
                path: "/aad/MemberCentre/Withdraw/CommonWithdraw",
                name: "aadCommonWithdraw",
                component: () =>
                  import(
                    "../components/memberCentre/withdraw/CommonWithdrawView.vue"
                  ),
              },
              {
                // 提款紀錄
                path: "/aad/MemberCentre/Withdraw/WithdrawRecord",
                name: "aadWithdrawRecord",
                component: () =>
                  import(
                    "../components/memberCentre/withdraw/WithdrawRecordView.vue"
                  ),
              },
              {
                // 綁定銀行卡
                path: "/aad/MemberCentre/Withdraw/WithdrawBankBind",
                name: "aadWithdrawBankBind",
                component: () =>
                  import(
                    "../components/memberCentre/withdraw/WithdrawBankBindView.vue"
                  ),
              },
              {
                // 修改轉帳密碼
                path: "/aad/MemberCentre/Withdraw/ChangeWithdrawPwView",
                name: "aadChangeWithdrawPwView",
                component: () =>
                  import(
                    "../components/memberCentre/withdraw/ChangeWithdrawPwView.vue"
                  ),
              },
            ],
          },
          {
            // VIP 特權
            path: "/aad/MemberCentre/VIP",
            name: "aadVIP",
            component: () => import("../components/memberCentre/VIPView.vue"),
          },
          {
            // 代理團隊
            path: "/aad/MemberCentre/Agency",
            name: "aadAgency",
            redirect: "/aad/MemberCentre/Agency/AgencyTeam", // 預設代理團隊
            component: () =>
              import("../components/memberCentre/agency/AgencyNavView.vue"),
            children: [
              {
                // 代理團隊
                path: "/aad/MemberCentre/Agency/AgencyTeam",
                name: "aadAgencyTeam",
                component: () =>
                  import(
                    "../components/memberCentre/agency/AgencyTeamView.vue"
                  ),
              },
              {
                // 成員管理
                path: "/aad/MemberCentre/Agency/AgencyMember",
                name: "aadAgencyMember",
                component: () =>
                  import(
                    "../components/memberCentre/agency/AgencyMemberView.vue"
                  ),
              },
              {
                // 代理推廣
                path: "/aad/MemberCentre/Agency/AgencyPromo",
                name: "aadAgencyPromo",
                component: () =>
                  import(
                    "../components/memberCentre/agency/AgencyPromoView.vue"
                  ),
              },
              {
                // 傭金報表
                path: "/aad/MemberCentre/Agency/AgencyRecord",
                name: "aadAgencyRecord",
                component: () =>
                  import(
                    "../components/memberCentre/agency/AgencyRecordView.vue"
                  ),
              },
              {
                // 傭金方案
                path: "/aad/MemberCentre/Agency/AgencyBonus",
                name: "aadAgencyBonus",
                component: () =>
                  import(
                    "../components/memberCentre/agency/AgencyBonusView.vue"
                  ),
              },
            ],
          },
          {
            // 歷史紀錄
            path: "/aad/MemberCentre/History",
            name: "aadHistory",
            redirect: "/aad/MemberCentre/History/HistoryStakeRecord", // 預設投注紀錄
            component: () =>
              import("../components/memberCentre/history/HistoryNavView.vue"),
            children: [
              {
                // 投注紀錄
                path: "/aad/MemberCentre/History/HistoryStakeRecord",
                name: "aadHistoryStakeRecord",
                component: () =>
                  import(
                    "../components/memberCentre/history/HistoryStakeRecordView.vue"
                  ),
              },
              {
                // 資金明細
                path: "/aad/MemberCentre/History/TransactionDetails",
                name: "aadTransactionDetails",
                component: () =>
                  import(
                    "../components/memberCentre/history/TransactionDetailsView.vue"
                  ),
              },
            ],
          },
          {
            // 站內信
            path: "/aad/MemberCentre/Message",
            children: [
              {
                // 站內信
                path: "",
                name: "aadMessage",
                component: () =>
                  import("../components/memberCentre/message/MessageView.vue"),
              },
              {
                // 站內信詳情
                path: "/aad/MemberCentre/Message/MessageDetail",
                name: "aadMessageDetail",
                component: () =>
                  import(
                    "../components/memberCentre/message/MessageDetailView.vue"
                  ),
              },
            ],
          },
        ],
      },
      {
        // 關於我們
        path: "/aad/OtherPage",
        name: "aadOtherPage",
        component: () => import("../pages/aad/OtherPage.vue"),
      },

      {
        // 下載
        path: "/aad/AppDownLoad",
        name: "aadAppDownLoad",
        component: () => import("../pages/aad/AppDownLoad.vue"),
      },
      {
        // 優惠活動
        path: "/aad/Promotion",
        name: "aadPromotion",
        component: () => import("../pages/aad/PromotionView.vue"),
      },
    ],
  },
  // aad 手機版
  {
    path: "/aad/phone",
    component: () => import("../layout/aad/PhoneView.vue"),
    beforeEnter: (to, from, next) => {
      if (layouts.includes("aad")) {
        const screenWidth = window.innerWidth;
        if (screenWidth >= 720) {
          next("/aad");
        } else {
          next();
        }
      }
      else {
        next("/");
      }
    },
    children: [
      {
        // 手機首頁
        path: "/aad/phone",
        name: "aadPhonehome",
        component: () => import("../pages/aad/phone/PhoneHomeView.vue"),
      },
      {
        // 遊戲大廳(手機版)
        path: "/aad/phone/PhoneGameHall",
        name: "aadPhoneGameHall",
        component: () => import("../pages/aad/phone/PhoneGameHallView.vue"),
      },
      {
        // 優惠(手機版)
        path: "/aad/phone/Promotion",
        name: "aadPhonePromotion",
        component: () => import("../pages/aad/phone/PhonePromotionView.vue"),
      },
      {
        // 會員中心(手機版)
        path: "/aad/phone/memberCentre",
        name: "aadPhoneMemberView",
        component: () =>
          import("../components/aad/phone/memberCentre/PhoneMemberView.vue"),
      },
      {
        // 充值首頁
        path: "/aad/phone/deposit/PhoneDepositNav",
        name: "aadPhoneDepositNav",
        component: () =>
          import(
            "../components/aad/phone/memberCentre/deposit/PhoneDepositNavView.vue"
          ),
      },
      {
        // 取款首頁
        path: "/aad/phone/withdraw/PhoneCommonWithdrawView",
        name: "aadPhoneCommonWithdraw",
        component: () =>
          import(
            "../components/aad/phone/memberCentre/withdraw/PhoneCommonWithdrawView.vue"
          ),
      },
      {
        // 資金明細(手機版)
        path: "/aad/phone/memberCentre/history/PhoneTransactionDetails",
        name: "aadPhoneTransactionDetails",
        component: () =>
          import(
            "../components/aad/phone/memberCentre/history/PhoneTransactionDetailsView.vue"
          ),
      },
      {
        // 投注紀錄(手機版)
        path: "/aad/phone/memberCentre/history/PhoneHistoryStakeRecord",
        name: "aadPhoneHistoryStakeRecord",
        component: () =>
          import(
            "../components/aad/phone/memberCentre/history/PhoneHistoryStakeRecordView.vue"
          ),
      },
      {
        // 選擇幣別(手機版)
        path: "/aad/phone/PhoneCurrencySelect",
        name: "aadPhoneCurrencySelect",
        component: () =>
          import("../components/aad/phone/PhoneCurrencySelectVIew.vue"),
      },
      {
        // 代理加盟(手機版)
        path: "/aad/phone/agency/PhoneAgency",
        name: "aadPhoneAgency",
        component: () =>
          import(
            "../components/aad/phone/memberCentre/agency/PhoneAgencyView.vue"
          ),
      },
      {
        // 代理加盟-成員管理(手機版)
        path: "/aad/phone/agency/PhoneAgencyTeam",
        name: "aadPhoneAgencyTeam",
        component: () =>
          import(
            "../components/aad/phone/memberCentre/agency/PhoneAgencyTeamView.vue"
          ),
      },
      {
        // 代理加盟-代理推廣(手機版)
        path: "/aad/phone/agency/PhoneAgencyPromo",
        name: "aadPhoneAgencyPromo",
        component: () =>
          import(
            "../components/aad/phone/memberCentre/agency/PhoneAgencyPromoView.vue"
          ),
      },
      {
        // 代理加盟-傭金方案(手機版)
        path: "/aad/phone/agency/PhoneAgencyBonus",
        name: "aadPhoneAgencyBonus",
        component: () =>
          import(
            "../components/aad/phone/memberCentre/agency/PhoneAgencyBonusView.vue"
          ),
      },
      {
        // 代理加盟-傭金領取紀錄(手機版)
        path: "/aad/phone/agency/PhoneAgencyBonusRecord",
        name: "aadPhoneAgencyBonusRecord",
        component: () =>
          import(
            "../components/aad/phone/memberCentre/agency/PhoneAgencyBonusRecordView.vue"
          ),
      },
      {
        // VIP頁(手機版)
        path: "/aad/phone/memberCentre/PhoneVIPView",
        name: "aadPhoneVIPView",
        component: () =>
          import("../components/aad/phone/memberCentre/PhoneVIPView.vue"),
      },
      {
        // 站內信(手機版)
        path: "/aad/phone/memberCentre/message/PhoneMessage",
        name: "aadPhoneMessage",
        component: () =>
          import(
            "../components/aad/phone/memberCentre/message/PhoneMessageView.vue"
          ),
      },
      {
        // 站內信內容頁(手機版)
        path: "/aad/phone/memberCentre/message/PhoneMessageDetail",
        name: "aadPhoneMessageDetail",
        component: () =>
          import(
            "../components/aad/phone/memberCentre/message/PhoneMessageDetailView.vue"
          ),
      },
      {
        // 設定
        path: "/aad/phone/memberCentre/userInfo/PhoneUserChangeInfo",
        name: "aadPhoneUserChangeInfoView",
        component: () =>
          import(
            "../components/aad/phone/memberCentre/userInfo/PhoneUserChangeInfoView.vue"
          ),
      },
    ],
  },

  // aae
  {
    path: "/aae",
    component: () => import("../layout/aae/LayoutView.vue"),
    beforeEnter: (to, from, next) => {
      if (layouts.includes("aae")) {
        const screenWidth = window.innerWidth;
        if (screenWidth < 720) {
          next("/aae/phone");
        } else {
          next();
        }
      }
      else {
        next("/");
      }
    },
    children: [
      {
        // 首頁
        path: "/aae",
        name: "aaeHome",
        component: () => import("../pages/aae/HomeView.vue"),
      },
      {
        // 體育
        path: "/aae/Sports",
        name: "aaeSports",
        component: () => import("../pages/aae/SportView.vue"),
      },
      {
        // 體育大廳
        path: "/aae/SportHall",
        name: "aaeSportHall",
        component: () => import("../pages/aae/SportHallView.vue"),
      },
      {
        // 真人
        path: "/aae/Casino",
        name: "aaeCasino",
        component: () => import("../pages/aae/CasinoView.vue"),
      },
      {
        // 真人大廳
        path: "/aae/CasinoHall",
        name: "aaeCasinoHall",
        component: () => import("../pages/aae/CasinoHallView.vue"),
      },
      {
        // 電子
        path: "/aae/Egame",
        name: "aaeEgame",
        component: () => import("../pages/aae/EgameView.vue"),
      },
      {
        // 彩票
        path: "/aae/Lottery",
        name: "aaeLottery",
        component: () => import("../pages/aae/LotteryView.vue"),
      },
      {
        // 彩票大廳
        path: "/aae/LotteryHall",
        name: "aaeLotteryHall",
        component: () => import("../pages/aae/LotteryHallView.vue"),
      },
      {
        // 捕魚
        path: "/aae/Fish",
        name: "aaeFish",
        component: () => import("../pages/aae/FishView.vue"),
      },
      {
        // 捕魚大廳
        path: "/aae/FishHall",
        name: "aaeFishHall",
        component: () => import("../pages/aae/FishHallView.vue"),
      },
      {
        // 棋牌
        path: "/aae/Chess",
        name: "aaeChess",
        component: () => import("../pages/aae/ChessView.vue"),
      },
      {
        // 運動
        path: "/aae/SportEvent",
        name: "aaeSportEvent",
        component: () => import("../pages/aae/SportEventView.vue"),
      },
      {
        // 運動大廳
        path: "/aae/SportEventHall",
        name: "aaeSportEventHall",
        component: () => import("../pages/aae/SportEventHallView.vue"),
      },
      {
        // 會員中心
        path: "/aae/MemberCentre",
        name: "aaeMemberCentre",
        component: () => import("../components/aae/MemberCentreView.vue"),
        children: [
          {
            // 帳戶與資料
            path: "/aae/MemberCentre/UserInfo",
            name: "aaeUserInfo",
            redirect: "/aae/MemberCentre/UserInfo/UserChangeInfo", // 預設基本資料
            component: () =>
              import("../components/memberCentre/userInfo/UserInfoNavView.vue"),
            children: [
              {
                // 基本資料
                path: "/aae/MemberCentre/UserInfo/UserChangeInfo",
                name: "aaeUserChangeInfo",
                component: () =>
                  import(
                    "../components/memberCentre/userInfo/UserChangeInfoView.vue"
                  ),
              },
              {
                // 修改密碼
                path: "/aae/MemberCentre/UserInfo/UserChangeLoginPw",
                name: "aaeUserChangeLoginPw",
                component: () =>
                  import(
                    "../components/memberCentre/userInfo/UserChangeLoginPwView.vue"
                  ),
              },
              {
                // 手機綁定
                path: "/aae/MemberCentre/UserInfo/BindPhoneNumber",
                name: "aaeBindPhoneNumber",
                component: () =>
                  import(
                    "../components/memberCentre/userInfo/BindPhoneNumber.vue"
                  ),
              },
            ],
          },
          {
            // 充值
            path: "/aae/MemberCentre/Deposit",
            name: "aaeDeposit",
            redirect: "/aae/MemberCentre/Deposit/CommonDeposit", // 預設傳統充值
            component: () =>
              import("../components/memberCentre/deposit/DepositNavView.vue"),
            children: [
              {
                // 傳統充值
                path: "/aae/MemberCentre/Deposit/CommonDeposit",
                name: "aaeCommonDeposit",
                component: () =>
                  import(
                    "../components/memberCentre/deposit/CommonDepositView.vue"
                  ),
              },
              {
                // 傳統充值紀錄
                path: "/aae/MemberCentre/Deposit/DepositRecord",
                name: "aaeDepositRecord",
                component: () =>
                  import(
                    "../components/memberCentre/deposit/DepositRecordView.vue"
                  ),
              },
              {
                // 線上充值
                path: "/aae/MemberCentre/Deposit/OnlineDeposit",
                name: "aaeOnlineDeposit",
                component: () =>
                  import(
                    "../components/memberCentre/deposit/OnlineDepositView.vue"
                  ),
              },
              {
                // 線上充值紀錄
                path: "/aae/MemberCentre/Deposit/OnlineDepositRecord",
                name: "aaeOnlineDepositRecord",
                component: () =>
                  import(
                    "../components/memberCentre/deposit/OnlineDepositRecordView.vue"
                  ),
              },
            ],
          },
          {
            // 提領
            path: "/aae/MemberCentre/Withdraw",
            name: "aaeWithdraw",
            redirect: "/aae/MemberCentre/Withdraw/CommonWithdraw", // 預設傳統提款
            component: () =>
              import("../components/memberCentre/withdraw/WithdrawNavView.vue"),
            children: [
              {
                // 傳統提款
                path: "/aae/MemberCentre/Withdraw/CommonWithdraw",
                name: "aaeCommonWithdraw",
                component: () =>
                  import(
                    "../components/memberCentre/withdraw/CommonWithdrawView.vue"
                  ),
              },
              {
                // 提款紀錄
                path: "/aae/MemberCentre/Withdraw/WithdrawRecord",
                name: "aaeWithdrawRecord",
                component: () =>
                  import(
                    "../components/memberCentre/withdraw/WithdrawRecordView.vue"
                  ),
              },
              {
                // 綁定銀行卡
                path: "/aae/MemberCentre/Withdraw/WithdrawBankBind",
                name: "aaeWithdrawBankBind",
                component: () =>
                  import(
                    "../components/memberCentre/withdraw/WithdrawBankBindView.vue"
                  ),
              },
              {
                // 修改轉帳密碼
                path: "/aae/MemberCentre/Withdraw/ChangeWithdrawPwView",
                name: "aaeChangeWithdrawPwView",
                component: () =>
                  import(
                    "../components/memberCentre/withdraw/ChangeWithdrawPwView.vue"
                  ),
              },
            ],
          },
          {
            // VIP 特權
            path: "/aae/MemberCentre/VIP",
            name: "aaeVIP",
            component: () => import("../components/memberCentre/VIPView.vue"),
          },
          {
            // 代理團隊
            path: "/aae/MemberCentre/Agency",
            name: "aaeAgency",
            redirect: "/aae/MemberCentre/Agency/AgencyTeam", // 預設代理團隊
            component: () =>
              import("../components/memberCentre/agency/AgencyNavView.vue"),
            children: [
              {
                // 代理團隊
                path: "/aae/MemberCentre/Agency/AgencyTeam",
                name: "aaeAgencyTeam",
                component: () =>
                  import(
                    "../components/memberCentre/agency/AgencyTeamView.vue"
                  ),
              },
              {
                // 成員管理
                path: "/aae/MemberCentre/Agency/AgencyMember",
                name: "aaeAgencyMember",
                component: () =>
                  import(
                    "../components/memberCentre/agency/AgencyMemberView.vue"
                  ),
              },
              {
                // 代理推廣
                path: "/aae/MemberCentre/Agency/AgencyPromo",
                name: "aaeAgencyPromo",
                component: () =>
                  import(
                    "../components/memberCentre/agency/AgencyPromoView.vue"
                  ),
              },
              {
                // 傭金報表
                path: "/aae/MemberCentre/Agency/AgencyRecord",
                name: "aaeAgencyRecord",
                component: () =>
                  import(
                    "../components/memberCentre/agency/AgencyRecordView.vue"
                  ),
              },
              {
                // 傭金方案
                path: "/aae/MemberCentre/Agency/AgencyBonus",
                name: "aaeAgencyBonus",
                component: () =>
                  import(
                    "../components/memberCentre/agency/AgencyBonusView.vue"
                  ),
              },
            ],
          },
          {
            // 歷史紀錄
            path: "/aae/MemberCentre/History",
            name: "aaeHistory",
            redirect: "/aae/MemberCentre/History/HistoryStakeRecord", // 預設投注紀錄
            component: () =>
              import("../components/memberCentre/history/HistoryNavView.vue"),
            children: [
              {
                // 投注紀錄
                path: "/aae/MemberCentre/History/HistoryStakeRecord",
                name: "aaeHistoryStakeRecord",
                component: () =>
                  import(
                    "../components/memberCentre/history/HistoryStakeRecordView.vue"
                  ),
              },
              {
                // 資金明細
                path: "/aae/MemberCentre/History/TransactionDetails",
                name: "aaeTransactionDetails",
                component: () =>
                  import(
                    "../components/memberCentre/history/TransactionDetailsView.vue"
                  ),
              },
            ],
          },
          {
            // 站內信
            path: "/aae/MemberCentre/Message",
            children: [
              {
                // 站內信
                path: "",
                name: "aaeMessage",
                component: () =>
                  import("../components/memberCentre/message/MessageView.vue"),
              },
              {
                // 站內信詳情
                path: "/aae/MemberCentre/Message/MessageDetail",
                name: "aaeMessageDetail",
                component: () =>
                  import(
                    "../components/memberCentre/message/MessageDetailView.vue"
                  ),
              },
            ],
          },
        ],
      },
      {
        // 關於我們
        path: "/aae/OtherPage",
        name: "aaeOtherPage",
        component: () => import("../pages/aae/OtherPage.vue"),
      },

      {
        // 下載
        path: "/aae/AppDownLoad",
        name: "aaeAppDownLoad",
        component: () => import("../pages/aae/AppDownLoad.vue"),
      },
      {
        // 優惠活動
        path: "/aae/Promotion",
        name: "aaePromotion",
        component: () => import("../pages/aae/PromotionView.vue"),
      },
    ],
  },
  // aae 手機版
  {
    path: "/aae/phone",
    component: () => import("../layout/aae/PhoneView.vue"),
    beforeEnter: (to, from, next) => {
      if (layouts.includes("aae")) {
        const screenWidth = window.innerWidth;
        if (screenWidth >= 720) {
          next("/aae");
        } else {
          next();
        }
      }
      else {
        next("/");
      }
    },
    children: [
      {
        // 手機首頁
        path: "/aae/phone",
        name: "aaePhonehome",
        component: () => import("../pages/aae/phone/PhoneHomeView.vue"),
      },
      {
        // 遊戲大廳(手機版)
        path: "/aae/phone/PhoneGameHall",
        name: "aaePhoneGameHall",
        component: () => import("../pages/aae/phone/PhoneGameHallView.vue"),
      },
      {
        // 優惠(手機版)
        path: "/aae/phone/Promotion",
        name: "aaePhonePromotion",
        component: () => import("../pages/aae/phone/PhonePromotionView.vue"),
      },
      {
        // 會員中心(手機版)
        path: "/aae/phone/memberCentre",
        name: "aaePhoneMemberView",
        component: () =>
          import("../components/aae/phone/memberCentre/PhoneMemberView.vue"),
      },
      {
        // 充值首頁
        path: "/aae/phone/deposit/PhoneDepositNav",
        name: "aaePhoneDepositNav",
        component: () =>
          import(
            "../components/aae/phone/memberCentre/deposit/PhoneDepositNavView.vue"
          ),
      },
      {
        // 取款首頁
        path: "/aae/phone/withdraw/PhoneCommonWithdrawView",
        name: "aaePhoneCommonWithdraw",
        component: () =>
          import(
            "../components/aae/phone/memberCentre/withdraw/PhoneCommonWithdrawView.vue"
          ),
      },
      {
        // 資金明細(手機版)
        path: "/aae/phone/memberCentre/history/PhoneTransactionDetails",
        name: "aaePhoneTransactionDetails",
        component: () =>
          import(
            "../components/aae/phone/memberCentre/history/PhoneTransactionDetailsView.vue"
          ),
      },
      {
        // 投注紀錄(手機版)
        path: "/aae/phone/memberCentre/history/PhoneHistoryStakeRecord",
        name: "aaePhoneHistoryStakeRecord",
        component: () =>
          import(
            "../components/aae/phone/memberCentre/history/PhoneHistoryStakeRecordView.vue"
          ),
      },
      {
        // 選擇幣別(手機版)
        path: "/aae/phone/PhoneCurrencySelect",
        name: "aaePhoneCurrencySelect",
        component: () =>
          import("../components/aae/phone/PhoneCurrencySelectVIew.vue"),
      },
      {
        // 代理加盟(手機版)
        path: "/aae/phone/agency/PhoneAgency",
        name: "aaePhoneAgency",
        component: () =>
          import(
            "../components/aae/phone/memberCentre/agency/PhoneAgencyView.vue"
          ),
      },
      {
        // 代理加盟-成員管理(手機版)
        path: "/aae/phone/agency/PhoneAgencyTeam",
        name: "aaePhoneAgencyTeam",
        component: () =>
          import(
            "../components/aae/phone/memberCentre/agency/PhoneAgencyTeamView.vue"
          ),
      },
      {
        // 代理加盟-代理推廣(手機版)
        path: "/aae/phone/agency/PhoneAgencyPromo",
        name: "aaePhoneAgencyPromo",
        component: () =>
          import(
            "../components/aae/phone/memberCentre/agency/PhoneAgencyPromoView.vue"
          ),
      },
      {
        // 代理加盟-傭金方案(手機版)
        path: "/aae/phone/agency/PhoneAgencyBonus",
        name: "aaePhoneAgencyBonus",
        component: () =>
          import(
            "../components/aae/phone/memberCentre/agency/PhoneAgencyBonusView.vue"
          ),
      },
      {
        // 代理加盟-傭金領取紀錄(手機版)
        path: "/aae/phone/agency/PhoneAgencyBonusRecord",
        name: "aaePhoneAgencyBonusRecord",
        component: () =>
          import(
            "../components/aae/phone/memberCentre/agency/PhoneAgencyBonusRecordView.vue"
          ),
      },
      {
        // VIP頁(手機版)
        path: "/aae/phone/memberCentre/PhoneVIPView",
        name: "aaePhoneVIPView",
        component: () =>
          import("../components/aae/phone/memberCentre/PhoneVIPView.vue"),
      },
      {
        // 站內信(手機版)
        path: "/aae/phone/memberCentre/message/PhoneMessage",
        name: "aaePhoneMessage",
        component: () =>
          import(
            "../components/aae/phone/memberCentre/message/PhoneMessageView.vue"
          ),
      },
      {
        // 站內信內容頁(手機版)
        path: "/aae/phone/memberCentre/message/PhoneMessageDetail",
        name: "aaePhoneMessageDetail",
        component: () =>
          import(
            "../components/aae/phone/memberCentre/message/PhoneMessageDetailView.vue"
          ),
      },
      {
        // 設定
        path: "/aae/phone/memberCentre/userInfo/PhoneUserChangeInfo",
        name: "aaePhoneUserChangeInfoView",
        component: () =>
          import(
            "../components/aae/phone/memberCentre/userInfo/PhoneUserChangeInfoView.vue"
          ),
      },
    ],
  },
  
  //aaf
  {
    path: "/aaf",
    component: () => import("../layout/aaf/LayoutView.vue"),
    beforeEnter: (to, from, next) => {
      if (layouts.includes("aaf")) {
        const screenWidth = window.innerWidth;
        if (screenWidth < 720) {
          next("/aaf/phone");
        } else {
          next();
        }
      }
      else {
        next("/");
      }
    },
    children: [
      {
        // 首頁
        path: "/aaf",
        name: "aafHome",
        component: () => import("../pages/aaf/HomeView.vue"),
      },
      {
        // Slot
        path: "/aaf/Slot",
        name: "aafSlot",
        component: () => import("../pages/aaf/SlotView.vue"),
      },
      {
        // Pescaria
        path: "/aaf/Pescaria",
        name: "aafPescaria",
        component: () => import("../pages/aaf/PescariaView.vue"),
      },
      {
        // Cartas
        path: "/aaf/Cartas",
        name: "aafCartas",
        component: () => import("../pages/aaf/CartasView.vue"),
      },
      {
        // Loteria
        path: "/aaf/Loteria",
        name: "aafLoteria",
        component: () => import("../pages/aaf/LoteriaView.vue"),
      },
      {
        // LiveCasino
        path: "/aaf/LiveCasino",
        name: "aafLiveCasino",
        component: () => import("../pages/aaf/LiveCasinoView.vue"),
      },
      {
        // Sport
        path: "/aaf/Sport",
        name: "aafSport",
        component: () => import("../pages/aaf/SportView.vue"),
      },
      {
        // Cock
        path: "/aaf/Cockfighiting",
        name: "aafCockfighiting",
        component: () => import("../pages/aaf/CockView.vue"),
      },
      {
        // 會員中心
        path: "/aaf/MemberCentre",
        name: "aafMemberCentre",
        component: () => import("../components/aaf/MemberCentreView.vue"),
        children: [
          {
            // 帳戶與資料
            path: "/aaf/MemberCentre/UserInfo",
            name: "aafUserInfo",
            redirect: "/aaf/MemberCentre/UserInfo/UserChangeInfo", // 預設基本資料
            component: () =>
              import("../components/memberCentre/userInfo/UserInfoNavView.vue"),
            children: [
              {
                // 基本資料
                path: "/aaf/MemberCentre/UserInfo/UserChangeInfo",
                name: "aafUserChangeInfo",
                component: () =>
                  import(
                    "../components/memberCentre/userInfo/UserChangeInfoView.vue"
                  ),
              },
              {
                // 修改密碼
                path: "/aaf/MemberCentre/UserInfo/UserChangeLoginPw",
                name: "aafUserChangeLoginPw",
                component: () =>
                  import(
                    "../components/memberCentre/userInfo/UserChangeLoginPwView.vue"
                  ),
              },
              {
                // 手機綁定
                path: "/aaf/MemberCentre/UserInfo/BindPhoneNumber",
                name: "aafBindPhoneNumber",
                component: () =>
                  import(
                    "../components/memberCentre/userInfo/BindPhoneNumber.vue"
                  ),
              },
            ],
          },
          {
            // 充值
            path: "/aaf/MemberCentre/Deposit",
            name: "aafDeposit",
            redirect: "/aaf/MemberCentre/Deposit/CommonDeposit", // 預設傳統充值
            component: () =>
              import("../components/memberCentre/deposit/DepositNavView.vue"),
            children: [
              {
                // 傳統充值
                path: "/aaf/MemberCentre/Deposit/CommonDeposit",
                name: "aafCommonDeposit",
                component: () =>
                  import(
                    "../components/memberCentre/deposit/CommonDepositView.vue"
                  ),
              },
              {
                // 傳統充值紀錄
                path: "/aaf/MemberCentre/Deposit/DepositRecord",
                name: "aafDepositRecord",
                component: () =>
                  import(
                    "../components/memberCentre/deposit/DepositRecordView.vue"
                  ),
              },
              {
                // 線上充值
                path: "/aaf/MemberCentre/Deposit/OnlineDeposit",
                name: "aafOnlineDeposit",
                component: () =>
                  import(
                    "../components/memberCentre/deposit/OnlineDepositView.vue"
                  ),
              },
              {
                // 線上充值紀錄
                path: "/aaf/MemberCentre/Deposit/OnlineDepositRecord",
                name: "aafOnlineDepositRecord",
                component: () =>
                  import(
                    "../components/memberCentre/deposit/OnlineDepositRecordView.vue"
                  ),
              },
            ],
          },
          {
            // 提領
            path: "/aaf/MemberCentre/Withdraw",
            name: "aafWithdraw",
            redirect: "/aaf/MemberCentre/Withdraw/CommonWithdraw", // 預設傳統提款
            component: () =>
              import("../components/memberCentre/withdraw/WithdrawNavView.vue"),
            children: [
              {
                // 傳統提款
                path: "/aaf/MemberCentre/Withdraw/CommonWithdraw",
                name: "aafCommonWithdraw",
                component: () =>
                  import(
                    "../components/memberCentre/withdraw/CommonWithdrawView.vue"
                  ),
              },
              {
                // 提款紀錄
                path: "/aaf/MemberCentre/Withdraw/WithdrawRecord",
                name: "aafWithdrawRecord",
                component: () =>
                  import(
                    "../components/memberCentre/withdraw/WithdrawRecordView.vue"
                  ),
              },
              {
                // 綁定銀行卡
                path: "/aaf/MemberCentre/Withdraw/WithdrawBankBind",
                name: "aafWithdrawBankBind",
                component: () =>
                  import(
                    "../components/memberCentre/withdraw/WithdrawBankBindView.vue"
                  ),
              },
              {
                // 修改轉帳密碼
                path: "/aaf/MemberCentre/Withdraw/ChangeWithdrawPwView",
                name: "aafChangeWithdrawPwView",
                component: () =>
                  import(
                    "../components/memberCentre/withdraw/ChangeWithdrawPwView.vue"
                  ),
              },
            ],
          },
          {
            // VIP 特權
            path: "/aaf/MemberCentre/VIP",
            name: "aafVIP",
            component: () => import("../components/memberCentre/VIPView.vue"),
          },
          {
            // 代理團隊
            path: "/aaf/MemberCentre/Agency",
            name: "aafAgency",
            redirect: "/aaf/MemberCentre/Agency/AgencyTeam", // 預設代理團隊
            component: () =>
              import("../components/memberCentre/agency/AgencyNavView.vue"),
            children: [
              {
                // 代理團隊
                path: "/aaf/MemberCentre/Agency/AgencyTeam",
                name: "aafAgencyTeam",
                component: () =>
                  import(
                    "../components/memberCentre/agency/AgencyTeamView.vue"
                  ),
              },
              {
                // 成員管理
                path: "/aaf/MemberCentre/Agency/AgencyMember",
                name: "aafAgencyMember",
                component: () =>
                  import(
                    "../components/memberCentre/agency/AgencyMemberView.vue"
                  ),
              },
              {
                // 代理推廣
                path: "/aaf/MemberCentre/Agency/AgencyPromo",
                name: "aafAgencyPromo",
                component: () =>
                  import(
                    "../components/memberCentre/agency/AgencyPromoView.vue"
                  ),
              },
              {
                // 傭金報表
                path: "/aaf/MemberCentre/Agency/AgencyRecord",
                name: "aafAgencyRecord",
                component: () =>
                  import(
                    "../components/memberCentre/agency/AgencyRecordView.vue"
                  ),
              },
              {
                // 傭金方案
                path: "/aaf/MemberCentre/Agency/AgencyBonus",
                name: "aafAgencyBonus",
                component: () =>
                  import(
                    "../components/memberCentre/agency/AgencyBonusView.vue"
                  ),
              },
            ],
          },
          {
            // 歷史紀錄
            path: "/aaf/MemberCentre/History",
            name: "aafHistory",
            redirect: "/aaf/MemberCentre/History/HistoryStakeRecord", // 預設投注紀錄
            component: () =>
              import("../components/memberCentre/history/HistoryNavView.vue"),
            children: [
              {
                // 投注紀錄
                path: "/aaf/MemberCentre/History/HistoryStakeRecord",
                name: "aafHistoryStakeRecord",
                component: () =>
                  import(
                    "../components/memberCentre/history/HistoryStakeRecordView.vue"
                  ),
              },
              {
                // 資金明細
                path: "/aaf/MemberCentre/History/TransactionDetails",
                name: "aafTransactionDetails",
                component: () =>
                  import(
                    "../components/memberCentre/history/TransactionDetailsView.vue"
                  ),
              },
            ],
          },
          {
            // 站內信
            path: "/aaf/MemberCentre/Message",
            children: [
              {
                // 站內信
                path: "",
                name: "aafMessage",
                component: () =>
                  import("../components/memberCentre/message/MessageView.vue"),
              },
              {
                // 站內信詳情
                path: "/aaf/MemberCentre/Message/MessageDetail",
                name: "aafMessageDetail",
                component: () =>
                  import(
                    "../components/memberCentre/message/MessageDetailView.vue"
                  ),
              },
            ],
          },
        ],
      },

    ]

  },

  // aaf 手機版
  {
    path: "/aaf/phone",
    component: () => import("../layout/aaf/PhoneView.vue"),
    name: "aaf",
    beforeEnter: (to, from, next) => {
      if (layouts.includes("aaf")) {
        const screenWidth = window.innerWidth;
        if (screenWidth >= 720) {
          next("/aaf");
        } else {
          next();
        }
      }
      else {
        next("/");
      }
    },
    children: [
      {
        // 手機首頁
        path: "/aaf/phone",
        name: "aafPhonehome",
        component: () => import("../pages/aaf/phone/PhoneHomeView.vue"),
      },
      {
        // 遊戲大廳(手機版)
        path: "/aaf/phone/PhoneGameHall",
        name: "aafPhoneGameHall",
        component: () => import("../pages/aaf/phone/PhoneGameHallView.vue"),
      },
      {
        // 優惠(手機版)
        path: "/aaf/phone/Promotion",
        name: "aafPhonePromotion",
        component: () => import("../pages/aaf/phone/PhonePromotionView.vue"),
      },
      {
        // 會員中心(手機版)
        path: "/aaf/phone/memberCentre",
        name: "aafPhoneMemberView",
        component: () =>
          import("../components/aaf/phone/memberCentre/PhoneMemberView.vue"),
      },
      {
        // 充值首頁
        path: "/aaf/phone/deposit/PhoneDepositNav",
        name: "aafPhoneDepositNav",
        component: () =>
          import(
            "../components/aaf/phone/memberCentre/deposit/PhoneDepositNavView.vue"
          ),
      },
      {
        // 取款首頁
        path: "/aaf/phone/withdraw/PhoneCommonWithdrawView",
        name: "aafPhoneCommonWithdraw",
        component: () =>
          import(
            "../components/aaf/phone/memberCentre/withdraw/PhoneCommonWithdrawView.vue"
          ),
      },
      {
        // 資金明細(手機版)
        path: "/aaf/phone/memberCentre/history/PhoneTransactionDetails",
        name: "aafPhoneTransactionDetails",
        component: () =>
          import(
            "../components/aaf/phone/memberCentre/history/PhoneTransactionDetailsView.vue"
          ),
      },
      {
        // 投注紀錄(手機版)
        path: "/aaf/phone/memberCentre/history/PhoneHistoryStakeRecord",
        name: "aafPhoneHistoryStakeRecord",
        component: () =>
          import(
            "../components/aaf/phone/memberCentre/history/PhoneHistoryStakeRecordView.vue"
          ),
      },
      {
        // 選擇幣別(手機版)
        path: "/aaf/phone/PhoneCurrencySelect",
        name: "aafPhoneCurrencySelect",
        component: () =>
          import("../components/aaf/phone/PhoneCurrencySelectVIew.vue"),
      },
      {
        // 代理加盟(手機版)
        path: "/aaf/phone/agency/PhoneAgency",
        name: "aafPhoneAgency",
        component: () =>
          import(
            "../components/aaf/phone/memberCentre/agency/PhoneAgencyView.vue"
          ),
      },
      {
        // 代理加盟-成員管理(手機版)
        path: "/aaf/phone/agency/PhoneAgencyTeam",
        name: "aafPhoneAgencyTeam",
        component: () =>
          import(
            "../components/aaf/phone/memberCentre/agency/PhoneAgencyTeamView.vue"
          ),
      },
      {
        // 代理加盟-代理推廣(手機版)
        path: "/aaf/phone/agency/PhoneAgencyPromo",
        name: "aafPhoneAgencyPromo",
        component: () =>
          import(
            "../components/aaf/phone/memberCentre/agency/PhoneAgencyPromoView.vue"
          ),
      },
      {
        // 代理加盟-傭金方案(手機版)
        path: "/aaf/phone/agency/PhoneAgencyBonus",
        name: "aafPhoneAgencyBonus",
        component: () =>
          import(
            "../components/aaf/phone/memberCentre/agency/PhoneAgencyBonusView.vue"
          ),
      },
      {
        // 代理加盟-傭金領取紀錄(手機版)
        path: "/aaf/phone/agency/PhoneAgencyBonusRecord",
        name: "aafPhoneAgencyBonusRecord",
        component: () =>
          import(
            "../components/aaf/phone/memberCentre/agency/PhoneAgencyBonusRecordView.vue"
          ),
      },
      {
        // VIP頁(手機版)
        path: "/aaf/phone/memberCentre/PhoneVIPView",
        name: "aafPhoneVIPView",
        component: () =>
          import("../components/aaf/phone/memberCentre/PhoneVIPView.vue"),
      },
      {
        // 站內信(手機版)
        path: "/aaf/phone/memberCentre/message/PhoneMessage",
        name: "aafPhoneMessage",
        component: () =>
          import(
            "../components/aaf/phone/memberCentre/message/PhoneMessageView.vue"
          ),
      },
      {
        // 站內信內容頁(手機版)
        path: "/aaf/phone/memberCentre/message/PhoneMessageDetail",
        name: "aafPhoneMessageDetail",
        component: () =>
          import(
            "../components/aaf/phone/memberCentre/message/PhoneMessageDetailView.vue"
          ),
      },
      {
        // 設定
        path: "/aaf/phone/memberCentre/userInfo/PhoneUserChangeInfo",
        name: "aafPhoneUserChangeInfoView",
        component: () =>
          import(
            "../components/aaf/phone/memberCentre/userInfo/PhoneUserChangeInfoView.vue"
          ),
      },
    ],
  },

  // aag
  {
    path: "/aag",
    component: () => import("../layout/aag/LayoutView.vue"),
    beforeEnter: (to, from, next) => {
      if (layouts.includes("aag")) {
        const screenWidth = window.innerWidth;
        if (screenWidth < 720) {
          next("/aag/phone");
        } else {
          next();
        }
      }
      else {
        next("/");
      }
    },
    children: [
      {
        // 首頁
        path: "/aag",
        name: "aagHome",
        component: () => import("../pages/aag/HomeView.vue"),
      },
      {
        // 體育
        path: "/aag/Sports",
        name: "aagSports",
        component: () => import("../pages/aag/SportView.vue"),
      },
      {
        // 體育大廳
        path: "/aag/SportHall",
        name: "aagSportHall",
        component: () => import("../pages/aag/SportHallView.vue"),
      },
      {
        // 真人
        path: "/aag/Casino",
        name: "aagCasino",
        component: () => import("../pages/aag/CasinoView.vue"),
      },
      {
        // 真人大廳
        path: "/aag/CasinoHall",
        name: "aagCasinoHall",
        component: () => import("../pages/aag/CasinoHallView.vue"),
      },
      {
        // 電子
        path: "/aag/Egame",
        name: "aagEgame",
        component: () => import("../pages/aag/EgameView.vue"),
      },
      {
        // 彩票
        path: "/aag/Lottery",
        name: "aagLottery",
        component: () => import("../pages/aag/LotteryView.vue"),
      },
      {
        // 彩票大廳
        path: "/aag/LotteryHall",
        name: "aagLotteryHall",
        component: () => import("../pages/aag/LotteryHallView.vue"),
      },
      {
        // 捕魚
        path: "/aag/Fish",
        name: "aagFish",
        component: () => import("../pages/aag/FishView.vue"),
      },
      {
        // 捕魚大廳
        path: "/aag/FishHall",
        name: "aagFishHall",
        component: () => import("../pages/aag/FishHallView.vue"),
      },
      {
        // 棋牌
        path: "/aag/Chess",
        name: "aagChess",
        component: () => import("../pages/aag/ChessView.vue"),
      },
      {
        // 運動
        path: "/aag/SportEvent",
        name: "aagSportEvent",
        component: () => import("../pages/aag/SportEventView.vue"),
      },
      {
        // 運動大廳
        path: "/aag/SportEventHall",
        name: "aagSportEventHall",
        component: () => import("../pages/aag/SportEventHallView.vue"),
      },
      {
        // 會員中心
        path: "/aag/MemberCentre",
        name: "aagMemberCentre",
        component: () => import("../components/aag/MemberCentreView.vue"),
        children: [
          {
            // 帳戶與資料
            path: "/aag/MemberCentre/UserInfo",
            name: "aagUserInfo",
            redirect: "/aag/MemberCentre/UserInfo/UserChangeInfo", // 預設基本資料
            component: () =>
              import("../components/memberCentre/userInfo/UserInfoNavView.vue"),
            children: [
              {
                // 基本資料
                path: "/aag/MemberCentre/UserInfo/UserChangeInfo",
                name: "aagUserChangeInfo",
                component: () =>
                  import(
                    "../components/memberCentre/userInfo/UserChangeInfoView.vue"
                  ),
              },
              {
                // 修改密碼
                path: "/aag/MemberCentre/UserInfo/UserChangeLoginPw",
                name: "aagUserChangeLoginPw",
                component: () =>
                  import(
                    "../components/memberCentre/userInfo/UserChangeLoginPwView.vue"
                  ),
              },
              {
                // 手機綁定
                path: "/aag/MemberCentre/UserInfo/BindPhoneNumber",
                name: "aagBindPhoneNumber",
                component: () =>
                  import(
                    "../components/memberCentre/userInfo/BindPhoneNumber.vue"
                  ),
              },
            ],
          },
          {
            // 充值
            path: "/aag/MemberCentre/Deposit",
            name: "aagDeposit",
            redirect: "/aag/MemberCentre/Deposit/CommonDeposit", // 預設傳統充值
            component: () =>
              import("../components/memberCentre/deposit/DepositNavView.vue"),
            children: [
              {
                // 傳統充值
                path: "/aag/MemberCentre/Deposit/CommonDeposit",
                name: "aagCommonDeposit",
                component: () =>
                  import(
                    "../components/memberCentre/deposit/CommonDepositView.vue"
                  ),
              },
              {
                // 傳統充值紀錄
                path: "/aag/MemberCentre/Deposit/DepositRecord",
                name: "aagDepositRecord",
                component: () =>
                  import(
                    "../components/memberCentre/deposit/DepositRecordView.vue"
                  ),
              },
              {
                // 線上充值
                path: "/aag/MemberCentre/Deposit/OnlineDeposit",
                name: "aagOnlineDeposit",
                component: () =>
                  import(
                    "../components/memberCentre/deposit/OnlineDepositView.vue"
                  ),
              },
              {
                // 線上充值紀錄
                path: "/aag/MemberCentre/Deposit/OnlineDepositRecord",
                name: "aagOnlineDepositRecord",
                component: () =>
                  import(
                    "../components/memberCentre/deposit/OnlineDepositRecordView.vue"
                  ),
              },
            ],
          },
          {
            // 提領
            path: "/aag/MemberCentre/Withdraw",
            name: "aagWithdraw",
            redirect: "/aag/MemberCentre/Withdraw/CommonWithdraw", // 預設傳統提款
            component: () =>
              import("../components/memberCentre/withdraw/WithdrawNavView.vue"),
            children: [
              {
                // 傳統提款
                path: "/aag/MemberCentre/Withdraw/CommonWithdraw",
                name: "aagCommonWithdraw",
                component: () =>
                  import(
                    "../components/memberCentre/withdraw/CommonWithdrawView.vue"
                  ),
              },
              {
                // 提款紀錄
                path: "/aag/MemberCentre/Withdraw/WithdrawRecord",
                name: "aagWithdrawRecord",
                component: () =>
                  import(
                    "../components/memberCentre/withdraw/WithdrawRecordView.vue"
                  ),
              },
              {
                // 綁定銀行卡
                path: "/aag/MemberCentre/Withdraw/WithdrawBankBind",
                name: "aagWithdrawBankBind",
                component: () =>
                  import(
                    "../components/memberCentre/withdraw/WithdrawBankBindView.vue"
                  ),
              },
              {
                // 修改轉帳密碼
                path: "/aag/MemberCentre/Withdraw/ChangeWithdrawPwView",
                name: "aagChangeWithdrawPwView",
                component: () =>
                  import(
                    "../components/memberCentre/withdraw/ChangeWithdrawPwView.vue"
                  ),
              },
            ],
          },
          {
            // VIP 特權
            path: "/aag/MemberCentre/VIP",
            name: "aagVIP",
            component: () => import("../components/memberCentre/VIPView.vue"),
          },
          {
            // 代理團隊
            path: "/aag/MemberCentre/Agency",
            name: "aagAgency",
            redirect: "/aag/MemberCentre/Agency/AgencyTeam", // 預設代理團隊
            component: () =>
              import("../components/memberCentre/agency/AgencyNavView.vue"),
            children: [
              {
                // 代理團隊
                path: "/aag/MemberCentre/Agency/AgencyTeam",
                name: "aagAgencyTeam",
                component: () =>
                  import(
                    "../components/memberCentre/agency/AgencyTeamView.vue"
                  ),
              },
              {
                // 成員管理
                path: "/aag/MemberCentre/Agency/AgencyMember",
                name: "aagAgencyMember",
                component: () =>
                  import(
                    "../components/memberCentre/agency/AgencyMemberView.vue"
                  ),
              },
              {
                // 代理推廣
                path: "/aag/MemberCentre/Agency/AgencyPromo",
                name: "aagAgencyPromo",
                component: () =>
                  import(
                    "../components/memberCentre/agency/AgencyPromoView.vue"
                  ),
              },
              {
                // 傭金報表
                path: "/aag/MemberCentre/Agency/AgencyRecord",
                name: "aagAgencyRecord",
                component: () =>
                  import(
                    "../components/memberCentre/agency/AgencyRecordView.vue"
                  ),
              },
              {
                // 傭金方案
                path: "/aag/MemberCentre/Agency/AgencyBonus",
                name: "aagAgencyBonus",
                component: () =>
                  import(
                    "../components/memberCentre/agency/AgencyBonusView.vue"
                  ),
              },
            ],
          },
          {
            // 歷史紀錄
            path: "/aag/MemberCentre/History",
            name: "aagHistory",
            redirect: "/aag/MemberCentre/History/HistoryStakeRecord", // 預設投注紀錄
            component: () =>
              import("../components/memberCentre/history/HistoryNavView.vue"),
            children: [
              {
                // 投注紀錄
                path: "/aag/MemberCentre/History/HistoryStakeRecord",
                name: "aagHistoryStakeRecord",
                component: () =>
                  import(
                    "../components/memberCentre/history/HistoryStakeRecordView.vue"
                  ),
              },
              {
                // 資金明細
                path: "/aag/MemberCentre/History/TransactionDetails",
                name: "aagTransactionDetails",
                component: () =>
                  import(
                    "../components/memberCentre/history/TransactionDetailsView.vue"
                  ),
              },
            ],
          },
          {
            // 站內信
            path: "/aag/MemberCentre/Message",
            children: [
              {
                // 站內信
                path: "",
                name: "aagMessage",
                component: () =>
                  import("../components/memberCentre/message/MessageView.vue"),
              },
              {
                // 站內信詳情
                path: "/aag/MemberCentre/Message/MessageDetail",
                name: "aagMessageDetail",
                component: () =>
                  import(
                    "../components/memberCentre/message/MessageDetailView.vue"
                  ),
              },
            ],
          },
        ],
      },
      {
        // 關於我們
        path: "/aag/OtherPage",
        name: "aagOtherPage",
        component: () => import("../pages/aag/OtherPage.vue"),
      },

      {
        // 下載
        path: "/aag/AppDownLoad",
        name: "aagAppDownLoad",
        component: () => import("../pages/aag/AppDownLoad.vue"),
      },
      {
        // 優惠活動
        path: "/aag/Promotion",
        name: "aagPromotion",
        component: () => import("../pages/aag/PromotionView.vue"),
      },
    ],
  },
  // aag 手機版
  {
    path: "/aag/phone",
    component: () => import("../layout/aag/PhoneView.vue"),
    beforeEnter: (to, from, next) => {
      if (layouts.includes("aag")) {
        const screenWidth = window.innerWidth;
        if (screenWidth >= 720) {
          next("/aag");
        } else {
          next();
        }
      }
      else {
        next("/");
      }
    },
    children: [
      {
        // 手機首頁
        path: "/aag/phone",
        name: "aagPhonehome",
        component: () => import("../pages/aag/phone/PhoneHomeView.vue"),
      },
      {
        // 遊戲大廳(手機版)
        path: "/aag/phone/PhoneGameHall",
        name: "aagPhoneGameHall",
        component: () => import("../pages/aag/phone/PhoneGameHallView.vue"),
      },
      {
        // 優惠(手機版)
        path: "/aag/phone/Promotion",
        name: "aagPhonePromotion",
        component: () => import("../pages/aag/phone/PhonePromotionView.vue"),
      },
      {
        // 會員中心(手機版)
        path: "/aag/phone/memberCentre",
        name: "aagPhoneMemberView",
        component: () =>
          import("../components/aag/phone/memberCentre/PhoneMemberView.vue"),
      },
      {
        // 充值首頁
        path: "/aag/phone/deposit/PhoneDepositNav",
        name: "aagPhoneDepositNav",
        component: () =>
          import(
            "../components/aag/phone/memberCentre/deposit/PhoneDepositNavView.vue"
          ),
      },
      {
        // 取款首頁
        path: "/aag/phone/withdraw/PhoneCommonWithdrawView",
        name: "aagPhoneCommonWithdraw",
        component: () =>
          import(
            "../components/aag/phone/memberCentre/withdraw/PhoneCommonWithdrawView.vue"
          ),
      },
      {
        // 資金明細(手機版)
        path: "/aag/phone/memberCentre/history/PhoneTransactionDetails",
        name: "aagPhoneTransactionDetails",
        component: () =>
          import(
            "../components/aag/phone/memberCentre/history/PhoneTransactionDetailsView.vue"
          ),
      },
      {
        // 投注紀錄(手機版)
        path: "/aag/phone/memberCentre/history/PhoneHistoryStakeRecord",
        name: "aagPhoneHistoryStakeRecord",
        component: () =>
          import(
            "../components/aag/phone/memberCentre/history/PhoneHistoryStakeRecordView.vue"
          ),
      },
      {
        // 選擇幣別(手機版)
        path: "/aag/phone/PhoneCurrencySelect",
        name: "aagPhoneCurrencySelect",
        component: () =>
          import("../components/aag/phone/PhoneCurrencySelectVIew.vue"),
      },
      {
        // 代理加盟(手機版)
        path: "/aag/phone/agency/PhoneAgency",
        name: "aagPhoneAgency",
        component: () =>
          import(
            "../components/aag/phone/memberCentre/agency/PhoneAgencyView.vue"
          ),
      },
      {
        // 代理加盟-成員管理(手機版)
        path: "/aag/phone/agency/PhoneAgencyTeam",
        name: "aagPhoneAgencyTeam",
        component: () =>
          import(
            "../components/aag/phone/memberCentre/agency/PhoneAgencyTeamView.vue"
          ),
      },
      {
        // 代理加盟-代理推廣(手機版)
        path: "/aag/phone/agency/PhoneAgencyPromo",
        name: "aagPhoneAgencyPromo",
        component: () =>
          import(
            "../components/aag/phone/memberCentre/agency/PhoneAgencyPromoView.vue"
          ),
      },
      {
        // 代理加盟-傭金方案(手機版)
        path: "/aag/phone/agency/PhoneAgencyBonus",
        name: "aagPhoneAgencyBonus",
        component: () =>
          import(
            "../components/aag/phone/memberCentre/agency/PhoneAgencyBonusView.vue"
          ),
      },
      {
        // 代理加盟-傭金領取紀錄(手機版)
        path: "/aag/phone/agency/PhoneAgencyBonusRecord",
        name: "aagPhoneAgencyBonusRecord",
        component: () =>
          import(
            "../components/aag/phone/memberCentre/agency/PhoneAgencyBonusRecordView.vue"
          ),
      },
      {
        // VIP頁(手機版)
        path: "/aag/phone/memberCentre/PhoneVIPView",
        name: "aagPhoneVIPView",
        component: () =>
          import("../components/aag/phone/memberCentre/PhoneVIPView.vue"),
      },
      {
        // 站內信(手機版)
        path: "/aag/phone/memberCentre/message/PhoneMessage",
        name: "aagPhoneMessage",
        component: () =>
          import(
            "../components/aag/phone/memberCentre/message/PhoneMessageView.vue"
          ),
      },
      {
        // 站內信內容頁(手機版)
        path: "/aag/phone/memberCentre/message/PhoneMessageDetail",
        name: "aagPhoneMessageDetail",
        component: () =>
          import(
            "../components/aag/phone/memberCentre/message/PhoneMessageDetailView.vue"
          ),
      },
      {
        // 設定
        path: "/aag/phone/memberCentre/userInfo/PhoneUserChangeInfo",
        name: "aagPhoneUserChangeInfoView",
        component: () =>
          import(
            "../components/aag/phone/memberCentre/userInfo/PhoneUserChangeInfoView.vue"
          ),
      },
    ],
  },
  
  //以下為單一頁面
  {
    // 註冊頁
    path: "/Register",
    name: "Register",
    component: () => import("../pages/RegisterView.vue"),
  },
  {
    // 跳轉遊戲 Load
    path: "/Load",
    name: "Load",
    component: () => import("../pages/LoadView.vue"),
  },
  {
    // 維護頁
    path: "/Maintain",
    name: "Maintain",
    component: () => import("../pages/MaintainView.vue"),
  },
  {
    // 測試頁
    path: "/Test",
    name: "Test",
    component: () => import("../pages/TestView.vue"),
  },
  {
    // 測試頁
    path: "/DeviceTest",
    name: "DeviceTest",
    component: () => import("../pages/DeviceTestView.vue"),
  },
  {
    // 測試頁
    path: "/googleLogin",
    name: "googleLogin",
    component: () => import("../pages/googleLogin.vue"),
  },  
  {
    // 測試頁
    path: "/facebookLogin",
    name: "facebookLogin",
    component: () => import("../pages/facebookLogin.vue"),
  }, 
  {
    // 單獨下載頁
    path: "/DownLoad",
    name: "DownLoad",
    component: () => import("../pages/DownLoad.vue"),
  },
  {
    // 真人 WS168
    path: "/Game",
    name: "Game",
    component: () => import("../pages/GameView.vue"),
  },
  {
    // 彈跳視窗開啟遊戲
    path: "/OpenGame",
    name: "OpenGame",
    component: () => import("../pages/OpenGameView.vue"),
  },
  {
    // 彈跳視窗開啟線上支付
    path: "/OnlinePay",
    name: "OnlinePay",
    component: () => import("../pages/OnlinePayView.vue"),
  },
  {
    path: "/:catchAll(.*)",
    redirect: "/",
    hidden: true,
  },

];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
  scrollBehavior() {
    // always scroll to top
    return { top: 0 };
  },
});

export default router;
