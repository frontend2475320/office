import { useStore } from "vuex";
import { useI18n } from "vue-i18n";
import { useRouter } from "vue-router";
import Swal from "sweetalert2";
import moment from "moment";
import apiService from '@/api/apiService';
import { useGamesStore } from '@/store/game'
import { computed } from 'vue';
import { startGame } from '@/api/api';
import { storeToRefs } from "pinia";

export default function () {
  const { t, locale } = useI18n();
  const router = useRouter();
  const store = useStore();
  const DomainLayoutId = computed(() => store.state.DomainLayoutId);
  const wallets = computed(() => store.state.wallets);
  const siteSettingsInfo = computed(() => store.state.siteSettingsInfo);
  const games = useGamesStore();

  const { gameMenu } = storeToRefs(games);
  // 錯誤處理
  function errorHandling(err) {
    if (err.response == null || err.response.status == null) {
      console.log(err);
      Swal.fire("", t("sportApiThirdPartyCode_systemError"), "error");
    } else {
      switch (err.response.status) {
        case 400: // 參數錯誤
          Swal.fire("", t("sportApiThirdPartyCode_parameterError"), "error");
          break;
        case 401: // Token 失效
          store.dispatch("setLogout");
          Swal.fire("", t("sportApiThirdPartyCode_invalidToken"), "info");
          router.push("/" + DomainLayoutId.value);
          break;
        case 403: // 沒有權限
          Swal.fire("", t("sportApiThirdPartyCode_notAuthorized"), "warning");
          break;
        case 459: // 第三方API錯誤
          Swal.fire("", t("third_party_api_error"), "warning");
          break;
        case 460: // IP 地址受限
          Swal.fire("", t("sportApiThirdPartyCode_iPAddressRestricted"), "warning");
          break;
        case 461: // 遊戲不支援此貨幣
          Swal.fire("", t("gameNoSupportCurrency"), "warning");
          break;
        case 499: // IP 系統維護
          Swal.fire("", t("sportApiThirdPartyCode_systemMaintenanceAPIIntegrationFile"), "warning");
          router.push("/Maintain");
          break;
        default: // 系統錯誤 (456:簽名錯誤、457:請求時間錯誤、458:Response錯誤)
          Swal.fire("", t("sportApiThirdPartyCode_systemError"), "error");
          break;
      }
    }
  }

  // 轉日期字串
  function unixToLocal(value) {
    return moment(value).local().format("YYYY-MM-DD HH:mm:ss");
  }

  // 轉日期字串
  function unixToDate(value) {
    return moment(value).local().format("YYYY-MM-DD");
  }

  function copyText(element, index) {
    const el = document.createElement('textarea');
    if (element) {
      el.value = element.innerHTML;
    }
    else {
      el.value = document.querySelector(`[data-copy-index="${index}"]`).textContent;
    }
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    Swal.fire("", t("copied_to_clipboard"), "success");

  }

  // 貨幣金額
  function stringToMoney(value) {
    switch (localStorage.getItem("locale")) {
      case "zh_chs":
      case "zh_cht":
        return Number(value).toLocaleString("zh-CN", {
          maximumFractionDigits: 2,
        });
      case "en":
        return Number(value).toLocaleString("en-US", {
          maximumFractionDigits: 2,
        });
      case "vn":
        return Number(value).toLocaleString("vi-VN", {
          maximumFractionDigits: 2,
        });
      default:
        return Number(value).toLocaleString("en-US", {
          maximumFractionDigits: 2,
        });
    }
  }

  // 錢包異動紀錄
  function walletTypeName(value) {
    switch (value) {
      case 1: // 入款
        return t("deposit");
      case 2: // 出款(審核中)
        return t("dispensing") + "(" + t("pendingReview") + ")";
      case 3: // 優惠活動
        return t("promotions");
      case 4: // 人工入款
        return t("manualDeposit");
      case 5: // 人工出款
        return t("manualWithdraw");
      case 6: // 投注
        return t("bet");
      case 7: // 派彩(結算)
        return t("payout");
      case 8: // 返水
        return t("retreat");
      case 9: // 返佣
        return t("rebate");
      case 10: // VIP相關
        return t("vip_privileges");
      case 11: // 出款(取消)
        return t("withdrawal_state_cancel");
      case 12: // 線上入款
        return t("onlineDeposit");
      case 13: // 線上取款
        return t("onlineWithdraw");
      case 14: // 線上取款(拒絕)
        return t("onlineWithdraw") + "(" + t("refuse") + ")";
      case 15: // 直播禮物
        return t("liveStreamGift");
      case 16: // anchorSalary
        return t("agreeToDeposit");
      case 17: // 同意下級入款
        return t("agreeToDeposit");
      case 18: // 紅包發送
        return t("red_packet_send");
      case 19: // 紅包發送返還
        return t("red_packet_return");
      case 20: // 領取紅包
        return t("red_packet_take");
      case 21: // 幣轉
        return t("currency_transfer");
      case 22: // 貴族爵位
        return t("peerage");
      case 23: // 遊戲館充值
        return t("game_deposit");
      case 24: // 遊戲館取款
        return t("game_withdrawal");
      case 25: // 優惠活動紅包
        return t("promotion_red_envelope");
      case 26: // 派彩(遊戲商活動)
        return t("payout_game_dealer_activity");
      case 27: // 合夥人返佣
        return t("partner_rebate");
      default:
        return "";
    }
  }

  // 使用者登出處理
  function userLogout() {
    store.dispatch("setLogout");
    Swal.fire("", t("sportApiThirdPartyCode_invalidToken"), "info");
    router.push("/" + DomainLayoutId.value);
  }

  // 取得站台資訊
  async function getSiteSettingInfo() {
    try {
      let res = await apiService.post('/SiteSetting/Info', {
        DomainLayoutId: DomainLayoutId.value
      })
      store.dispatch('setSiteSettingsInfo', res.data);
    } catch (err) {
      errorHandling(err);
    }
  }

  async function setCurrencyToLocalStorage() {
   let matched= siteSettingsInfo.value.domainLayoutCurrencyInfos.find(item => item.no === +localStorage.getItem('currencyNo'))

    if (!localStorage.getItem('currencyNo') || localStorage.getItem('currencyNo') == "null" || !matched) {
      localStorage.setItem('currencyNo', siteSettingsInfo.value.domainLayoutCurrencyInfos[0].no);
    }
    if (!localStorage.getItem('currencyId') || localStorage.getItem('currencyId') != "null") {
      localStorage.setItem('currencyId', siteSettingsInfo.value.domainLayoutCurrencyInfos[0].currencyId);
    }
  }

  async function getUserInfo() {
    try {
      let resp = await apiService.post('/User/Info', {
        sessionKey: localStorage.getItem("sessionKey"),
        lang: localStorage.getItem('locale'),
        DomainLayoutId: DomainLayoutId.value
      });
      store.dispatch('setUserData', resp.data);
      localStorage.setItem('DomainLayoutId', DomainLayoutId.value);
    } catch (err) {
      errorHandling(err);
    }
  }

  function getWalletsAll() {
    return [
      {
        no: 1,
        image: require('@/assets/img/currency/CNY.svg')
      },
      {
        no: 2,
        image: require('@/assets/img/currency/USD.svg')
      },
      {
        no: 3,
        image: require('@/assets/img/currency/IDR.svg')
      },
      {
        no: 4,
        image: require('@/assets/img/currency/MYR.svg')
      },
      {
        no: 5,
        image: require('@/assets/img/currency/VND.svg')
      },
      {
        no: 6,
        image: require('@/assets/img/currency/THB.svg')
      },
      {
        no: 7,
        image: require('@/assets/img/currency/USDT_ERC20.svg')
      },
      {
        no: 8,
        image: require('@/assets/img/currency/USDT_TRC20.svg')
      },
      {
        no: 9,
        image: require('@/assets/img/currency/ETH.svg')
      },
      {
        no: 10,
        image: require('@/assets/img/currency/USDC_ERC20.svg')
      },
      {
        no: 11,
        image: require('@/assets/img/currency/TRX.svg')
      },
      {
        no: 12,
        image: require('@/assets/img/currency/DOGE.svg')
      },
      {
        no: 13,
        image: require('@/assets/img/currency/VND.svg')
      },
      {
        no: 14,
        image: require('@/assets/img/currency/BRL.svg')
      },
      {
        no: 15,
        image: require('@/assets/img/currency/JPY.svg')
      },
      {
        no: 16,
        image: require('@/assets/img/currency/KRW.svg')
      },
      {
        no: 17,
        image: require('@/assets/img/currency/INR.svg')
      },
    ]
  }

  function getLocaleAll() {
    return [
      {
        key: 'zh_chs',
        name: 'CN',
        image: require("@/assets/img/lang/lang-cn.png"),
      },
      {
        key: 'zh_cht',
        name: 'ZH',
        image: require("@/assets/img/lang/lang-hg.png"),
      },
      {
        key: 'en',
        name: 'EN',
        image: require("@/assets/img/lang/lang-en.png"),
      },
      {
        key: 'vn',
        name: 'VN',
        image: require("@/assets/img/lang/lang-vn.png"),
      },
      {
        key: 'pt',
        name: 'BR',
        image: require("@/assets/img/lang/lang-bri.png"),
      },
      {
        key: 'ja_jp',
        name: 'JP',
        image: require("@/assets/img/lang/lang_jp.svg"),
      },
      {
        key: 'hi_in',
        name: 'IN',
        image: require("@/assets/img/lang/lang_india.svg"),
      },
      {
        key: 'id_id',
        name: 'ID',
        image: require("@/assets/img/lang/lang_indonesia.svg"),
      },
    ]
  }

  function getRecRegisterUrl(recommendCode) {
    return window.location.host + '/#/register?code=' + recommendCode + '&lang=' + localStorage.getItem("locale");
  }

  // 選擇註冊 或 登入
  function selectTab(value) {
    store.dispatch('serShowLoginTab', value);
  }

  //登出
  function logout() {
    store.dispatch('setLogout')
    if (window.location.href.includes("MemberCentre")) {
      router.push("/" + DomainLayoutId.value)
    }
  }
  //取得錢包列表
  async function getWallets() {
    const walletsAll = getWalletsAll();
    try {
      let resp = await apiService.post('/Wallet/List', {
        sessionKey: localStorage.getItem("sessionKey"),
        DomainLayoutId: DomainLayoutId.value
      });

      let handledWallets = [];
      resp.data.data.forEach(wallet => {
        wallet.currency.img = walletsAll.find(item => item.no === wallet.currency.no).image;
        if (wallet.currency.protocol != null)
          wallet.currency.currencyId += "(" + wallet.currency.protocol + ")";

        handledWallets.push(wallet);
      })
      store.dispatch("setWallets", handledWallets)
    } catch (err) {
      errorHandling(err);
    }
  }
  //選擇錢包
  function selectWallet(wallet) {
    let walletsCurrencyNoArray = [wallet.currency.no];
    wallets.value.forEach(wallet => {
      walletsCurrencyNoArray.push(wallet.currency.no);
    })
    walletsCurrencyNoArray = [...new Set(walletsCurrencyNoArray)];

    const params = {
      sessionKey: localStorage.getItem("sessionKey"),
      currencyNo: walletsCurrencyNoArray,
      DomainLayoutId: DomainLayoutId.value
    };
    apiService.put('/Wallet/Sort/Update', params).then(() => {
      getWallets();
    }).catch((err) => {
      errorHandling(err);
    });
  }
  // 更新錢包按鈕觸發
  async function refreshWallets(refreshIcon) {
    refreshIcon.classList.add("motion");
    try {
      let resp = await apiService.post('/Wallet/List', {
        sessionKey: localStorage.getItem("sessionKey"),
        DomainLayoutId: DomainLayoutId.value
      });

      resp.data.data.forEach(data => {
        wallets.value.forEach(wallet => {
          if(data.currency.no === wallet.currency.no){
            wallet.balance = data.balance
          }
        })
      })
    } catch (err) {
      errorHandling(err);
    }
    refreshIcon.classList.remove("motion");
  }

  //切換語系
  function changeLang(lang) {
    localStorage.setItem('locale', lang.key);
    locale.value = lang.key;
    store.dispatch('setLang', lang);
  }

  //設定語系
  function setLang(defaultLang) {
    const matchedLangs = computed(() => store.state.matchedLangs);
    let lang;
    if (!localStorage.getItem('locale')){
      localStorage.setItem('locale', defaultLang.key);
    }
    else{
      let lang = matchedLangs.value.find(lang => lang.key === localStorage.getItem('locale'))
      if(!lang){
        localStorage.setItem('locale', defaultLang.key);
      }
    }
    locale.value = localStorage.getItem('locale');
    lang = matchedLangs.value.find(lang => lang.key === localStorage.getItem('locale'))
    store.dispatch('setLang', lang);
    
  }

  async function getGames(gameListMethod) {
    switch (gameListMethod) {
      case "getGameList":
        await games.getGameList(t, DomainLayoutId.value);
        break;
      case "getPhoneGameList":
        await games.getPhoneGameList(t, DomainLayoutId.value);
        break;
    }
  }

  async function getLogo(deviceType, placeId) {
    return await getBillboard(deviceType, placeId)
  }

  async function getPhoneBanner(deviceType, placeId) {
    return await getBillboards(deviceType, placeId)
  }

  async function getBonusImages(deviceType, placeId) {
    return await getBillboards(deviceType, placeId)
  }

  async function getPhoneAgencyBanner(deviceType, placeId) {
    return await getBillboards(deviceType, placeId)
  }

  async function getBillboards(deviceType, placeId) {
    const params = {
      deviceType: deviceType, //0=app 1=pc
      lang: localStorage.getItem("locale") ?? 'en',
      DomainLayoutId: DomainLayoutId.value
    }
    try {
      let resp = await apiService.post('/Billboard/List', params);
      let imgsArray = [];
      resp.data.datas.forEach(element => {
        if (element.placeId === placeId) {
          imgsArray.push(element);
        }
      });


      return imgsArray;
    }
    catch (err) {
      errorHandling(err)
    }
  }

  async function getBillboard(deviceType, placeId) {
    const params = {
      deviceType: deviceType, //0=app 1=pc
      lang: localStorage.getItem("locale") ?? 'en',
      DomainLayoutId: DomainLayoutId.value
    }
    try {
      let resp = await apiService.post('/Billboard/List', params);
      return resp.data.datas.find(item => item.placeId === placeId);
    }
    catch (err) {
      console.log(err)
    }
  }


  function getMatchedLangList() {
    let matchedLangs = [];
    const localeAll = getLocaleAll();
    siteSettingsInfo.value.domainLayoutLangInfos.forEach(element => {
      if(localeAll.find(item => item.key === element.langId)){
        matchedLangs.push(localeAll.find(item => item.key === element.langId));
      }
    });
    store.dispatch('setMatchedLangs', matchedLangs);
  }

  function setWebNameAndIco() {
    document.title = siteSettingsInfo.value.webTitle;

    let link = document.createElement('link');
    link.rel = 'icon';
    document.head.appendChild(link);
    link.href = siteSettingsInfo.value.webTitleIco;
  }

  async function getCountryCodeList() {
    let countryCodeList = [];
    const params = {
      lang: localStorage.getItem("locale") ?? 'en',
    }
    try {
      let resp = await apiService.post('/SiteSetting/CountryCodeList', params);
      resp.data.datas.forEach((item) => {
        countryCodeList.push(item)
      })

      return countryCodeList;
    }
    catch (err) {
      console.log(err)
    }
  }

  //點擊gameThirdParty
  async function clickGameThirdParty(gameThirdParty, routerPath, isLoadingObj){
    // status 狀態(0=啟用,1=停用,2=維護,3=下架)
    if (gameThirdParty.status === 0) {
      await games.setGameThirdPartyNo(localStorage.getItem('gameClass'), gameThirdParty.gameThirdPartyNo, gameThirdParty.id, 'gameHall');
      // clickAction 點擊動作(1=進入遊戲列表,2=直接跳轉遊戲商大廳)
      if (gameThirdParty.clickAction === 2) {
        isLoadingObj.value = true;
        try {
          await startGame(locale.value, gameThirdParty.gameThirdPartyNo, null, null);
        } catch (err) {
          if (err.response == null || err.response.status == null) {
            console.log(err);
            Swal.fire("", t("sportApiThirdPartyCode_systemError"), "error");
          } else if (err.response.status == 460) // 遊戲維護
            Swal.fire("", t("game_maintain"), "warning");
          else errorHandling(err);
        }
        isLoadingObj.value = false;
      } else {
        router.push(routerPath);
      }
    }
  }
  
  async function getGameThirdPartyGames(gameThirdPartyNo, isLoadingObj){
    const params = {
      lang: localStorage.getItem("locale"),
      gameThirdPartyNo,
    }
    try {
      isLoadingObj.value = true;
      let resp = await apiService.post('/Game/Game/List', params);
      const gameThirdPartyNo = resp.data.datas[0]?.gameThirdPartyNo;
      const gameInfos = resp.data.datas[0]?.gameInfos;
      


      gameMenu.value.forEach(gameThirdParty => {
        if(gameThirdParty.gameThirdPartyNo === gameThirdPartyNo){
          gameThirdParty.games = [...gameInfos];
        }
      }) 
    }
    catch (err) {
      console.log(err)
    } 
    isLoadingObj.value = false;
  }

  return { errorHandling, unixToLocal, unixToDate, copyText, stringToMoney, walletTypeName, userLogout, getSiteSettingInfo, setCurrencyToLocalStorage, getUserInfo, getWalletsAll, getLocaleAll, getRecRegisterUrl, selectTab, logout, getWallets, selectWallet, refreshWallets, changeLang, setLang, getLogo, getMatchedLangList, setWebNameAndIco, getPhoneBanner, getBillboards, getBonusImages, getPhoneAgencyBanner, getGames, getCountryCodeList, clickGameThirdParty, getGameThirdPartyGames};
}
