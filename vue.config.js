const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  publicPath: './',
  configureWebpack:{
    optimization: {
      splitChunks: {
        chunks: 'all',
        minSize: 200000,
        maxSize: 1500000,
      },
    },
  },
  chainWebpack: (config) => {
    config.resolve.alias
      .set('vue-i18n', 'vue-i18n/dist/vue-i18n.cjs.js')
      .end();
    config.plugin('define').tap((args) => {
      args[0]['process.env']['VERSION'] = JSON.stringify(require('./package.json').version);
      return args;
    });
  },
})
