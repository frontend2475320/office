#!/bin/bash

# Set club configuration
cp /app/prod/wking/environments/config3.js /usr/share/nginx/html
# Service up
/usr/sbin/nginx

# Container keeplive
#/bin/ping localhost
/usr/bin/tail -f /var/log/nginx/access.log
